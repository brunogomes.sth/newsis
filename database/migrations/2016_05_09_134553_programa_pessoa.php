<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProgramaPessoa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PROGRAMA_PESSOA', function (Blueprint $table) {
            $table->increments('ID_PROGRAMA_PESSOA');
            $table->integer('ID_PESSOA');
            $table->integer('ID_VERSION');
            $table->integer('ID_USUARIO')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('PROGRAMA_PESSOA');
    }
}
