<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pessoa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PESSOA', function (Blueprint $table) {
            $table->increments('ID_PESSOA');
            $table->char('TP_PESSOA',1)->nullable();
            $table->string('CD_PESSOA', 25)->nullable();
            $table->string('NM_PESSOA', 150)->nullable();
            $table->integer('ID_CIDADE')->nullable(); //SAIU
            $table->string('NM_BAIRRO', 150)->nullable(); //SAIU
            $table->string('NM_LOGRADOURO', 150)->nullable(); //SAIU
            $table->string('NN_NUMERO', 10)->nullable();
            $table->string('DS_COMPLEMENTO', 50)->nullable();
            $table->dateTime('DT_CADASTRO')->nullable();
            $table->string('DS_OBS', 300)->nullable();
            $table->integer('ID_GRUPO_CLIENTE')->nullable();
            $table->string('DS_BASE', 300)->nullable();
            $table->boolean('FL_ATIVO')->nullable();
            $table->boolean('FL_DAR_SENHA')->nullable();
            $table->boolean('FL_FUNCIONARIO')->nullable();
            $table->boolean('FL_CLIENTE')->nullable();
            $table->dateTime('DT_FINAL_GRATIS')->nullable();
            $table->dateTime('DT_INICIAL_GRATIS')->nullable();
            $table->boolean('FL_CONTRATO')->nullable();
            $table->char('PRAZO', 2)->nullable();
            $table->string('DS_CEP', 8)->nullable(); //SAIU
            $table->integer('CD_VEND_REPR')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('PESSOA');
    }
}
