<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterandoPessoaRemovendoEndereco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('PESSOA', function( $table ){

            $table->dropColumn(['NM_LOGRADOURO','NM_BAIRRO',
                                'ID_CIDADE','DS_CEP',]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
