<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AtendimentoUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ATENDIMENTO_USUARIO', function (Blueprint $table) {
            $table->increments('ID_ATEND_USUARIO');
            $table->integer('ID_ATENDIMENTO');
            $table->integer('ID_USUARIO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ATENDIMENTO_USUARIO');
    }
}
