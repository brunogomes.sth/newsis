<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterandoProgramaClienteUnicaVersao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('PROGRAMA_PESSOA', function($table){


            $table->dropPrimary('ID_PROGRAMA_PESSOA')->change();
            $table->integer('ID_VERSION')->unsigned()->change();
            $table->integer('ID_PESSOA')->unsigned()->change();

            $table->primary(['ID_PESSOA', 'ID_VERSION']);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
