<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Operacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('OPERACAO', function (Blueprint $table) {
            $table->increments('ID_OPERACAO');
            $table->string('CD_OPERACAO', 10);
            $table->string('NM_OPERACAO', 50);
            $table->string('DS_OPERACAO', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('OPERACAO');
    }
}
