<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Senha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SENHA', function (Blueprint $table) {
            $table->increments('ID_SENHA');
            $table->integer('ID_PESSOA')->nullable();
            $table->string('DS_SENHA_ENT', 50)->nullable();
            $table->string('DS_SENHA_SAI', 50)->nullable();
            $table->dateTime('DT_REGISTRO')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('SENHA');
    }
}
