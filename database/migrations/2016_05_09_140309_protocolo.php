<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Protocolo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PROTOCOLO', function (Blueprint $table) {
            $table->increments('ID_PROTOCOLO');
            $table->integer('ID_ATENDIMENTO')->nullable();
            $table->dateTime('DT_PROTOCOLO')->nullable();
            $table->string('DS_REMETENTE', 50)->nullable();
            $table->string('DS_DESTINATARIO', 50)->nullable();
            $table->integer('ID_PESSOA')->nullable();
            $table->string('DS_HISTORICO')->nullable();
            $table->integer('ID_USUARIO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('PROTOCOLO');
    }
}
