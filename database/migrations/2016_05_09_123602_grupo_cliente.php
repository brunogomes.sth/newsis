<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrupoCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GRUPO_CLIENTE', function (Blueprint $table) {
            $table->increments('ID_GRUPO_CLIENTE');
            $table->string('NM_GRUPO_CLIENTE', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GRUPO_CLIENTE');
    }
}
