<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Programa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PROGRAMA', function (Blueprint $table) {
            $table->increments('ID_PROGRAMA');
            $table->string('NM_PROGRAMA', 50);
            $table->dateTime('DT_CADASTRO');
            $table->string('DS_PLATAFORM', 25);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('PROGRAMA');
    }
}
