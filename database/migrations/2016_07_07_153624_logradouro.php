<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Logradouro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LOGRADOURO', function (Blueprint $table){

            $table->increments('ID_LOGRADOURO');
            $table->string('NM_LOGRADOURO', 150)->nullable();
            $table->string('DS_CEP', 8)->nullable();
            $table->integer('ID_BAIRRO');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
