<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Atendimento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ATENDIMENTO', function (Blueprint $table) {
            $table->increments('ID_ATENDIMENTO');
            $table->string('CD_ATENDIMENTO', 20);
            $table->dateTime('DT_ATENDIMENTO');
            $table->integer('ID_PESSOA');
            $table->integer('ID_USUARIO');
            $table->integer('ID_STATUS');
            $table->integer('ID_PRAZO');
            $table->string('DS_OBS');
            $table->integer('ID_USUARIO_ALT');
            $table->dateTime('DT_ALTERACAO')->nullable();
            $table->dateTime('DT_TERMINO')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ATENDIMENTO');
    }
}
