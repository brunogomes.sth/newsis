<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrupoSegOperacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GRUPO_SEG_OPERACAO', function (Blueprint $table) {
            $table->increments('ID_GRUPO_OPERACAO');
            $table->integer('ID_GRUPO_SEG');
            $table->integer('ID_OPERACAO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GRUPO_SEG_OPERACAO');
    }
}
