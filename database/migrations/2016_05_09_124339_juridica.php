<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Juridica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('JURIDICA', function (Blueprint $table) {
            $table->increments('ID_JURIDICA');
            $table->string('NM_FANTASIA', 150)->nullable();
            $table->char('CD_CNPJ', 14);
            $table->string('CD_CGF', 50)->nullable();
            $table->string('NM_RESPONSAVEL', 150)->nullable();
            $table->integer('ID_PESSOA');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('JURIDICA');
    }
}
