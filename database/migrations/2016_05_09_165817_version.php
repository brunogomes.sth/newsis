<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Version extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('VERSION', function (Blueprint $table) {
            $table->increments('ID_VERSION');
            $table->string('DS_VERSION', 250);
            $table->integer('ID_PROGRAMA');
            $table->text('DS_HISTORICO')->nullable();;
            $table->date('DT_VERSION')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('VERSION');
    }
}
