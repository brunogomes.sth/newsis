<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fisica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FISICA', function (Blueprint $table) {
            $table->increments('ID_FISICA');
            $table->integer('ID_PESSOA');
            $table->char('CD_CPF', 11);
            $table->string('CD_RG', 25)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('FISICA');
    }
}
