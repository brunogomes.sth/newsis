<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AtendimentoGrupo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ATENDIMENTO_GRUPO', function (Blueprint $table) {
            $table->increments('ID_ATEND_GRUPO');
            $table->integer('ID_ATENDIMENTO');
            $table->integer('ID_GRUPO_SEG');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ATENDIMENTO_GRUPO');
    }
}
