<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterandoFuncionarioRemovendoRgECpf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('FUNCIONARIO', function ($table){
            $table->dropColumn(['APELIDO','CD_CPF','CD_RG','DT_NASC']);
            $table->integer('ID_FISICA')->create();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
