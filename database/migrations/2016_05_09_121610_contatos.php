<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contatos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CONTATOS', function (Blueprint $table) {
            $table->increments('ID_CONTATO');
            $table->string('NM_CONTATO', 50)->nullable();
            $table->string('DS_TEL_01', 11)->nullable();
            $table->string('DS_TEL_02', 11)->nullable();
            $table->string('DS_CEL_01', 11)->nullable();
            $table->string('DS_CEL_02', 11)->nullable();
            $table->string('DS_EMAIL', 100)->nullable();
            $table->char('TP_PESSOA_CONTATO', 1)->nullable();
            $table->integer('ID_PESSOA')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CONTATOS');
    }
}
