<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsuarioGrupoSeg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('USUARIO_GRUPO_SEG', function (Blueprint $table) {
            $table->increments('ID_USUARIO_GRUPO');
            $table->integer('ID_USUARIO');
            $table->integer('ID_GRUPO_SEG');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('USUARIO_GRUPO_SEG');
    }
}
