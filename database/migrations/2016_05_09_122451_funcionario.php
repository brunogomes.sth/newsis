<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Funcionario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FUNCIONARIO', function (Blueprint $table) {
            $table->increments('ID_FUNCIONARIO');
            $table->integer('ID_PESSOA');
            $table->integer('ID_SETOR');
            $table->double('COMISSAO')->nullable();
            $table->string('APELIDO', 50)->nullable();
            $table->char('CD_CPF', 11);
            $table->string('CD_RG', 25)->nullable();
            $table->date('DT_NASC');
            $table->integer('ID_CARGO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('FUNCIONARIO');
    }
}
