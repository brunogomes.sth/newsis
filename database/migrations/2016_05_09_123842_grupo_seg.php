<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrupoSeg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GRUPO_SEG', function (Blueprint $table) {
            $table->increments('ID_GRUPO_SEG');
            $table->string('NM_GRUPO_SEG', 50);
            $table->string('DS_GRUPO_SEG', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('GRUPO_SEG');
    }
}
