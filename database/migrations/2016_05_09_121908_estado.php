<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Estado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        
        Schema::create('ESTADO', function (Blueprint $table) {
            $table->increments('ID_ESTADO');
            $table->string('NM_ESTADO', 30);
            $table->char('SG_ESTADO', 2);
            $table->string('DS_PAIS', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ESTADO');
    }
}
