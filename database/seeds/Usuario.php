<?php

use Illuminate\Database\Seeder;

class Usuario extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('CIDADE')->truncate();

        $usuarios[] = array(
            array(
                "name" => "Admin",
                "username"=>"admin",
                "email" => "jbg.bruno@gmail.com",
                "password" => bcrypt('admin1')
            )
        );
        foreach ($usuarios as $usuario) {
            $this->command->info('Inserindo Usuarios'.' ['.  count($usuarios).']..');
            \App\User::insert($usuario);
        }
    }
}
