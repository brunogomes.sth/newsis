<?php

use Illuminate\Database\Seeder;

class Estados extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ESTADO')->truncate();
        $ufs[] = array(
            array("ID_ESTADO" => "1", "NM_ESTADO" => "Ceará", "SG_ESTADO" => "CE", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "2", "NM_ESTADO" => "Paraíba", "SG_ESTADO" => "PB", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "3", "NM_ESTADO" => "Pernambuco", "SG_ESTADO" => "PE", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "4", "NM_ESTADO" => "Rio de Janeiro", "SG_ESTADO" => "RJ", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "5", "NM_ESTADO" => "São Paulo", "SG_ESTADO" => "SP", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "6", "NM_ESTADO" => "Distrito Federal", "SG_ESTADO" => "DF", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "7", "NM_ESTADO" => "Maranhão", "SG_ESTADO" => "MA", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "8", "NM_ESTADO" => "Pará", "SG_ESTADO" => "PA", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "9", "NM_ESTADO" => "Acre", "SG_ESTADO" => "AC", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "10", "NM_ESTADO" => "Alagoas", "SG_ESTADO" => "AL", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "11", "NM_ESTADO" => "Amazonas", "SG_ESTADO" => "AM", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "12", "NM_ESTADO" => "Bahia", "SG_ESTADO" => "BA", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "13", "NM_ESTADO" => "Espírito Santo", "SG_ESTADO" => "ES", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "14", "NM_ESTADO" => "Goiás", "SG_ESTADO" => "GO", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "15", "NM_ESTADO" => "Amapá", "SG_ESTADO" => "AP", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "16", "NM_ESTADO" => "Mato Grosso", "SG_ESTADO" => "MT", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "17", "NM_ESTADO" => "Mato Grosso do Sul", "SG_ESTADO" => "MS", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "18", "NM_ESTADO" => "Minas Gerais", "SG_ESTADO" => "MG", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "19", "NM_ESTADO" => "Piauí", "SG_ESTADO" => "PI", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "20", "NM_ESTADO" => "Rio Grande do Norte", "SG_ESTADO" => "RN", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "21", "NM_ESTADO" => "Rio Grande do Sul", "SG_ESTADO" => "RS", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "22", "NM_ESTADO" => "Rondônia", "SG_ESTADO" => "RO", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "23", "NM_ESTADO" => "Roraima", "SG_ESTADO" => "RR", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "24", "NM_ESTADO" => "Santa Catarina", "SG_ESTADO" => "SC", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "25", "NM_ESTADO" => "Sergipe", "SG_ESTADO" => "SE", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "26", "NM_ESTADO" => "Tocantins", "SG_ESTADO" => "TO", "DS_PAIS" => "Brasil"),
            array("ID_ESTADO" => "27", "NM_ESTADO" => "Paraná", "SG_ESTADO" => "PR", "DS_PAIS" => "Brasil"),
        );
        foreach ($ufs as $uf) {
            $this->command->info('Inserindo Estados'.' ['.  count($uf).']..');
            \App\Estado::insert($uf);
        }
    }
}
