<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Contatos;
use Illuminate\Support\Facades\DB;

class ContatosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('contatos')->truncate();
        $faker = Faker::create();
        foreach (range(1, 30) as $i) {
        Contatos::create(
            [
                'NM_CONTATO'   => $faker->name(),
                'DS_TEL_01'     => $faker->ean8(),
                'DS_TEL_02'     => $faker->ean8(),
                'DS_CEL_01'     => $faker->ean8(),
                'DS_CEL_02'     => $faker->ean8(),
                'DS_EMAIL'      => $faker->email()
            ]
        );
        }
    }
}
