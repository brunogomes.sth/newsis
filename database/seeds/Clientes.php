<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Pessoa;
use Illuminate\Support\Facades\DB;
use Faker\Provider\pt_BR\Person as FakerBR;

class Clientes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pessoa')->truncate();
        $faker = Faker::create();
        $fakerBR = new FakerBR();

        $tipo_fisica = true;
        foreach (range(1, 30) as $i) {
            Pessoa::create(
                [
                    'NM_PESSOA'   => $faker->name(),
                    'TP_PESSOA'=> $tipo_fisica?'1':'2',
                    'ID_CIDADE'=>$faker->numberBetween(100,10000),
                    'NM_BAIRRO'=> $faker->word(),
                    'NM_LOGRADOURO'=> $faker->address(),
                    'NN_NUMERO'=> $faker->numberBetween(1,5000),
                    'DS_COMPLEMENTO'=> $faker->word(),
                    'DS_EMAIL'=> $faker->email(),
                    'DS_OBS'=> $faker->text(200),
                    'DS_BASE'=> $faker->text(200),
                    'FL_ATIVO'=> $faker->boolean(),
                    'FL_DAR_SENHA'=> $faker->boolean(),
                    'FL_CLIENTE'=> '1',
                    'DS_TEL_01'     => $faker->ean8(),
                    'DS_TEL_02'     => $faker->ean8(),
                    'DS_CELULAR_01'     => $faker->ean8(),
                    'DS_CELULAR_02'     => $faker->ean8(),
                    'FL_CONTRATO'     => $faker->boolean(),
                    'DS_CEP'     => $faker->ean8()
                ]
            );
            if ($tipo_fisica){
                \App\Fisica::create(
                [
                    'CD_CPF'=> $fakerBR->cpf(false),
                    'CD_RG'=> $fakerBR->rg()
                ]
                );
            }else{
               \App\Juridica::create([
                   
               ]); 
            }
            $tipo_fisica = !$tipo_fisica;
        }
    }
}
