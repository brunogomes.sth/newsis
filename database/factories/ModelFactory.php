<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/



$factory->define(App\User::class, function (Faker\Generator $faker) {
	return [
	'name' => $faker->name,
	'email' => $faker->email,
	'password' => bcrypt(str_random(10)),
	'remember_token' => str_random(10),
	];
});


$factory->define(App\Pessoa::class, function(Faker\Generator $faker){
	return[

	'TP_PESSOA' => 1,
	'CD_PESSOA' => null,
	'NM_PESSOA' => $faker->name,
	'NN_NUMERO' => $faker->postcode,
	'DS_COMPLEMENTO' => $faker->secondaryAddress,
	'DT_CADASTRO' => null,
	'DS_OBS' => $faker->text(50),
	'ID_GRUPO_CLIENTE'  => 1,
	'DS_BASE' => $faker->text(10),
	'FL_ATIVO' => (bool) rand(0,1),
	'FL_DAR_SENHA' => (bool) rand(0,1),
	'FL_FUNCIONARIO' => false,
	'FL_CLIENTE' => true,
	'DT_FINAL_GRATIS' => null,
	'DT_INICIAL_GRATIS' => null,
	'FL_CONTRATO' => (bool) mt_rand(0,1),
	'PRAZO' => function()
	{
		$p = [02 ,13 , 26, 52 ];
		return $p[rand(0,3)]; 
	},
	'CD_VEND_REPR' => null,
	'ID_LOGRADOURO' => function()
					{
						return factory(App\Logradouro::class)->create()->ID_LOGRADOURO;
					},
	];
});


$factory->define(App\Bairro::class, function(Faker\Generator $faker){

	return [
	'ID_CIDADE' => 13110,
	'NM_BAIRRO' => $faker->city,
	];
});

$factory->define(App\Logradouro::class, function(Faker\Generator $faker){
	return[
	'NM_LOGRADOURO' => $faker->streetName,
	'DS_CEP' => mt_rand(10000000,99999999),
	'ID_BAIRRO' => function() 
				{
					return factory(App\Bairro::class)->create()->ID_BAIRRO;
				},
	];
});

$factory->define(App\Contatos::class, function(Faker\Generator $faker,$d){

	return[
	'NM_CONTATO' => $faker->name,
	'DS_TEL_01' => '88' . mt_rand(1, 999999999),
	'DS_TEL_02' => '88' . mt_rand(1, 999999999),
	'DS_CEL_01' => '88' . mt_rand(1, 999999999),
	'DS_CEL_02' => '88' . mt_rand(1, 999999999),
	'DS_EMAIL' => $faker->email,
	'TP_PESSOA_CONTATO' => 'C',
	'ID_PESSOA' => $d['ID_PESSOA'],
	];
});

$factory->define(App\Fisica::class, function(Faker\Generator $faker,$d){

	return[
		'ID_PESSOA' => $d['ID_PESSOA'],
  		'CD_CPF'    => '64087346692' ,
  		'CD_RG' 	=> '12313131313131',
		'DT_NASC'	=> date('Y-m-d'),
		'APELIDO' 	=> $faker->name
	];
});