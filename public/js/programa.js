$(document).ready(function () {
    $('.data').mask('00/00/0000');
});

$('#ModalVersao').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var nome = button.data('nome').split("|").join(" ");
    var modal = $(this);
    modal.find('.modal-body .id').val(id);
    modal.find('.modal-body .nome').val(nome);
});

$('#ModalExcluir').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var url = button.data('url');
    var tipo = button.data('excluir');
    var nome = button.data('nome');
    var modal = $(this);
    modal.find('.modal-title').text('Excluir ' + tipo + '?');
    modal.find('.modal-conteudo').text('Excluir ' + tipo + ' ' + nome);
    if (tipo == 'Versão') {
        modal.find('#confirma').attr('href', url + "/programas/" + id + "/delV");
        modal.find('.modal-obs').text('');
    } else {
        modal.find('#confirma').attr('href', url + "/programas/" + id + "/delP");
        modal.find('.modal-obs').text('Obs.: Ao excluir o Programa também será excluído todas as versões correspondente a ele.');
    }
});

var idp = $("#cdprograma").val();
var idv = $("#cdversao").val();
if (idp > 0) {
    $('.versao').attr('disabled', 'disabled');
} else if (idv > 0) {
    $('.programa').attr('disabled', 'disabled');
}
    