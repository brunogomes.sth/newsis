function getCookie(e) {
    return document.cookie.length > 0 && (c_start = document.cookie.indexOf(e + "="), -1 != c_start) ? (c_start = c_start + e.length + 1, c_end = document.cookie.indexOf(";", c_start), -1 == c_end && (c_end = document.cookie.length), unescape(document.cookie.substring(c_start, c_end))) : ""
}
function Ajax() {
    try {
        return new XMLHttpRequest
    } catch (e) {
        try {
            return new ActiveXObject("Microsoft.XMLHTTP")
        } catch (e) {
            alert("O seu navegador não suporta AJAX.")
        }
    }
}
function GAE(e, t, a) {
    try {
        ga("send", "event", e, t, a)
    } catch (o) {
    }
}
function QtdAdd(e) {
    "" == getCookie("qtd") ? e ? createCookie("qtd", 0, 365) : createCookie("qtd", 1, 365) : createCookie("qtd", parseInt(getCookie("qtd")) + 1, 365)
}
function UsuarioIDGet() {
    return "" == getCookie("UsuarioID").trim() ? (fIDUsuario = Math.round(1e9 * Math.random()), createCookie("UsuarioID", fIDUsuario, 365), fIDUsuario) : getCookie("UsuarioID")
}
function SorteioCriar() {
    Sorteio = [];
    for (var e = 0; e < Servidores.length; e++)for (var t = 0; t < Servidores[e].prioridade; t++)Sorteio.push(e)
}
function ServidoresAdd(e, t, a) {
    var o = Ajax();
    o.onreadystatechange = function () {
        4 == this.readyState && (200 == this.status ? this.responseText.indexOf("OK") > -1 ? (Servidores[0].prioridade -= a, Servidores.push({
            nome: e,
            url: t,
            prioridade: a
        }), SorteioCriar(), GAE("Server2", "OK", "OK")) : GAE("Server2", "Não OK", this.responseText) : GAE("Server2", "Não 200", this.status))
    }, o.open("GET", t + "&t=teste&r=" + Math.round(1e9 * Math.random()), !0), o.send()
}
function MyRandom(e, t) {
    return total = -1 * e + t, Math.round(total / 100 * (100 * Math.random()) - -1 * e)
}
function Sorteando() {
    var e = MyRandom(0, Sorteio.length), t = Servidores[Sorteio[e]];
    Servidor = t.url
}
function ele(e) {
    return document.getElementById(e)
}
function ir(e) {
    document.location = e
}
function opcao(e) {
    ele("nfe").className = "opcao", ele("cte").className = "opcao", e.className = "opcao opcao-marcado", BaixarCaptcha()
}
function strapenasstr(e, t) {
    var a, o, n = "";
    for (a = 0; a <= e.length - 1; a++)for (o = 0; o <= t.length - 1; o++)e.substr(a, 1) == t.substr(o, 1) && (n += e.substr(a, 1), o = t.length);
    return n
}
function strnumeros(e) {
    return strapenasstr(e, "0123456789")
}
function TipoNFeSet(e) {
    e ? ele("nfe").click() : ele("cte").click()
}
function TipoNFeIs() {
    return ele("nfe").className.indexOf("opcao-marcado") > -1
}
function Tipo() {
    return null != ele("nfe") ? TipoNFeIs() ? "&cte=0" : "&cte=1" : "&alternativo=1"
}
function URL() {
    var e = "11";
    return "OK" == ele("pub").value && (e = hash()), Servidor + "&UsuarioID=" + UsuarioID + Tipo() + "&pub=" + e + "&com=" + hash()
}
function GetClass(e) {
    return document.getElementsByClassName(e)
}
function MsgF() {
    document.onkeydown = null;
    var e = GetClass("msg-f");
    e.length > -1 && (e = e[e.length - 1], e.parentNode.removeChild(e));
    var e = GetClass("msg");
    e.length > -1 && (e = e[e.length - 1], e.parentNode.removeChild(e))
}
function MsgOK(e) {
    MsgF(), null != e && e()
}
function MsgSim(e) {
    MsgF(), null != e && e()
}
function MsgNao(e) {
    MsgF(), null != e && e()
}
function HtmlC(e) {
    var t = document.createDocumentFragment(), a = document.createElement("div");
    for (a.innerHTML = e; a.firstChild;)t.appendChild(a.firstChild);
    document.body.insertBefore(t, document.body.childNodes[0])
}
function MsgB(e, t) {
    document.onkeydown = function (e) {
        e = e || window.event, 27 == e.keyCode && MsgF()
    };
    var a = document.createElement("div");
    a.className = "msg-f";
    var o = document.createElement("div");
    o.className = "msg";
    var n = document.createElement("div");
    if (o.appendChild(n), null == t)n.style.borderBottomStyle = "none"; else {
        n.style.padding = "15px", n.style.borderBottom = "1px solid #e5e5e5";
        var r = document.createElement("div");
        r.style.cssFloat = "right", r.style.borderBottomStyle = "none", r.style.padding = "15px";
        for (var i = 0; i < t.length; i++)r.appendChild(t[i]);
        o.appendChild(r)
    }
    n.innerHTML = e, document.body.appendChild(a), document.body.appendChild(o)
}
function MsgInf(e, t) {
    var a = document.createElement("button");
    a.id = "msgok", a.className = "msg-but", a.innerHTML = "OK", a.addEventListener("click", function () {
        MsgOK(t)
    }), MsgB(e, [a]), ele("msgok").focus()
}
function MsgQue(e, t, a) {
    var o = document.createElement("button");
    o.className = "but but-red", o.innerHTML = "Não", o.style.marginRight = "4px", o.addEventListener("click", function () {
        MsgOK(a)
    });
    var n = document.createElement("button");
    n.id = "msgsim", n.className = "but", n.innerHTML = "Sim", n.addEventListener("click", function () {
        MsgOK(t)
    }), MsgB(e, [o, n]), ele("msgsim").focus()
}
function loadMostra(e) {
    e ? (null != ele("load") && (ele("load").style.display = "block"), null != ele("loadf") && (ele("loadf").style.display = "block")) : (null != ele("load") && (ele("load").style.display = "none"), null != ele("loadf") && (ele("loadf").style.display = "none"))
}
function BaixarCaptchaFim() {
    loadMostra(!1), 44 == strnumeros(ele("chave").value).length ? ele("captcha").focus() : ele("chave").focus()
}
function ContarCaracters() {
    ele("qtd").innerHTML = "Qtd.Caracteres: " + strnumeros(ele("chave").value).length
}
function LeitorBarras(e) {
    1 != e.ctrlKey || "74" != e.which && "74" != e.which || (e.preventDefault(), ele("captcha").focus())
}
function Consultado(e) {
    null != ele("consultado") && (e ? (ele("consultado").style.display = "table", ele("chave").disabled = !0, ele("captcha").disabled = !0, ele("butbaixar").className = "but-destivado", ele("butconsulta").className = "but-destivado") : (ele("consultado").style.display = "none", ele("chave").disabled = !1, ele("captcha").disabled = !1, ele("butbaixar").className = "", ele("butconsulta").className = ""))
}
function NovaConsulta() {
    Consultado(!1), ele("chave").value = "", ele("captcha").value = "", GAE("Consulta Nota", "Nova"), BaixarCaptcha()
}
function Verifica() {
    var e = ChaveNum(), t = ele("captcha").value;
    if ("" == e)return MsgInf("É necessário digitar a chave.", function () {
        ele("chave").focus()
    }), !1;
    if (44 != e.length)return MsgInf("A chave precisa ter 44 dígitos.", function () {
        ele("chave").focus()
    }), !1;
    if (null != ele("nfe")) {
        if ("55" == e.substr(20, 2) && 0 == TipoNFeIs())return MsgInf("Essa chave e do tipo NFe. O sistema mudará para o modo NFe. Digite o código da imagem ao lado novamente.", function () {
            TipoNFeSet(!0)
        }), !1;
        if ("57" == e.substr(20, 2) && 1 == TipoNFeIs())return MsgInf("Essa chave e do tipo CTe. O sistema mudará para o modo CTe. Digite o código da imagem ao lado novamente.", function () {
            TipoNFeSet(!1)
        }), !1
    }
    return "" == t ? (MsgInf("É necessário digitar o código da imagem ao lado.", function () {
        ele("chave").focus()
    }), !1) : t.length < 4 ? (MsgInf("O código da imagem ao lado tem que ter 6 caracteres, favor digitar novamente.", function () {
        ele("chave").focus()
    }), !1) : !0
}
function BaixarCaptcha(e) {
    Sorteando(), Consultado(!1), 0 != e && loadMostra(!0), ele("imgcaptcha").src = URL() + "&t=captcha&chave=" + ChaveNum() + "&r=" + Math.round(1e9 * Math.random()), ele("captcha").value = "", BaixarCaptchaIniciado && GAE("Consulta Nota", "Captcha"), BaixarCaptchaIniciado = !0
}
function Consultar() {
    if (1 == Verifica()) {
        loadMostra(!0);
        var e = Ajax();
        e.onreadystatechange = function () {
            if (4 == this.readyState && 200 == this.status) {
                loadMostra(!1);
                var e = this.responseText.trim();
                if ("OK" == e)Consultado(!0), GAE("Consulta Nota", "Consulta", "Consultado"), QtdAdd(); else if ("Código da Imagem inválido. Tente novamente." == e)MsgInf(e, function () {
                    ele("captcha").focus(), BaixarCaptcha(!0)
                }), GAE("Consulta Nota", "Consulta", "Captcha Errado"); else if (GAE("Consulta Nota", "Consulta", e), e.indexOf("NF-e INEXISTENTE na base nacional") > -1 || e.indexOf("CT-e INEXISTENTE na base nacional") > -1) {
                    var t = "";
                    e.indexOf("NF-e INEXISTENTE na base nacional") > -1 && (t = 'NF-e INEXISTENTE na base nacional, em caso de dúvida consulte diretamente no portal oficial nacional de NFe.<a href="#" onclick="window.open(\'http://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=&nfe=' + ChaveNum() + "');\">http://www.nfe.fazenda.gov.br/</a>", "35" != ChaveNum().substr(0, 2) && "31" != ChaveNum().substr(0, 2) || (t += '<br/><br/>Você também pode testar utilizando o portal de teste do site FSist que utiliza a base de dados alternativa.<div style="text-align: right"><input class="msg-but" type="button" value="IR PARA O PORTAL DE TESTE"onclick="window.open(\'?PortalAlternativo\');"/></div>')), e.indexOf("CT-e INEXISTENTE na base nacional") > -1 && (t = 'CT-e INEXISTENTE na base nacional, em caso de dúvida consulte diretamente no portal oficial nacional de CTe.<a href="#" onclick="window.open(\'http://www.cte.fazenda.gov.br/consulta.aspx?tipoConsulta=completa&tipoConteudo=mCK/KoCqru0==&cte=' + ChaveNum() + "');\">http://www.cte.fazenda.gov.br/</a>"), MsgInf(t, function () {
                        BaixarCaptcha(!0)
                    })
                } else MsgInf(e, function () {
                    BaixarCaptcha(!0)
                })
            }
        }, e.open("GET", URL() + "&t=consulta&chave=" + ChaveNum() + "&captcha=" + ele("captcha").value, !0), e.send()
    }
}
function Visualizar() {
    window.open(URL() + "&t=visualizar&chave=" + ChaveNum()), GAE("Consulta Nota", "Visualizar")
}
function Imprimir() {
    window.open(URL() + "&t=pdf&chave=" + ChaveNum()), GAE("Consulta Nota", "PDF")
}
function XMLSemCert() {
    document.location = URL() + "&t=xmlsemcert&chave=" + ChaveNum(), GAE("Consulta Nota", "Sem Certificado")
}
function XMLComCert() {
    document.location = URL() + "&t=xmlcomcert&chave=" + ChaveNum(), GAE("Consulta Nota", "Com Certificado")
}
function hash() {
    function e(e, t) {
        var a, o, n = _$_1b9b[0];
        for (a = 0; a <= e[_$_1b9b[1]] - 1; a++)for (o = 0; o <= t[_$_1b9b[1]] - 1; o++)e[_$_1b9b[2]](a, 1) == t[_$_1b9b[2]](o, 1) && (n += e[_$_1b9b[2]](a, 1), o = t[_$_1b9b[1]]);
        return n
    }

    function t(t) {
        return e(t, _$_1b9b[3])
    }

    function a(e, t) {
        function a(e, t) {
            var a = e + t;
            return a > 255 && (a -= 256), 0 > a && (a += 256), a
        }

        for (var o = _$_1b9b[0], n = 0; n < e[_$_1b9b[1]]; n++) {
            for (var r = e[n][_$_1b9b[4]](), i = 0; i < t[_$_1b9b[1]]; i++)r = a(r, t[i][_$_1b9b[4]]());
            o += parseInt(r)[_$_1b9b[5]](16)
        }
        return o
    }

    return a(document[_$_1b9b[8]](_$_1b9b[7])[_$_1b9b[6]], ele(_$_1b9b[7])[_$_1b9b[6]] + t(ele(_$_1b9b[9])[_$_1b9b[6]]))
}
"function" != typeof String.prototype.trim && (String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "")
});
var createCookie = function (e, t, a) {
    var o;
    if (a) {
        var n = new Date;
        n.setTime(n.getTime() + 24 * a * 60 * 60 * 1e3), o = "; expires=" + n.toGMTString()
    } else o = "";
    document.cookie = e + "=" + t + o + "; path=/"
}, UsuarioID = UsuarioIDGet(), Servidor = "baixarxml.ashx?fsist=1", ChaveNum = function () {
    return strnumeros(ele("chave").value)
}, Servidores = [{nome: "Servidor 1", url: "baixarxml.ashx?m=WEB", prioridade: 100}], Sorteio = [];
SorteioCriar(), Number.prototype.format = function (e, t, a, o) {
    var n = "\\d(?=(\\d{" + (t || 3) + "})+" + (e > 0 ? "\\D" : "$") + ")", r = this.toFixed(Math.max(0, ~~e));
    return (o ? r.replace(".", o) : r).replace(new RegExp(n, "g"), "$&" + (a || ","))
};
var MsgOnSim = null, MsgOnNao = null, BaixarCaptchaIniciado = !1, _$_1b9b = ["", "length", "substr", "5426319870", "charCodeAt", "toString", "value", "captcha", "getElementById", "chave"];
null != ele("imgcaptcha") && imgcaptcha.addEventListener("load", BaixarCaptchaFim), setTimeout(function () {
    ServidoresAdd("Servidor 2", "https://server2.fsist.com.br/baixarxml.ashx?m=WEB", 40)
}, 1e3), setTimeout(function () {
    ServidoresAdd("Servidor 3", "https://server3.fsist.com.br/baixarxml.ashx?m=WEB", 40)
}, 1e3), QtdAdd(!0);