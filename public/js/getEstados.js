function cidade(state, city, url) {

	var option = '';
	
	$("#cidade").html('<option>Carregando...</option>');


	$.get(url +"/"+ state, null, function (json) {

		$("#cidade").html('<option value="">Selecione</option>');

		$.each(json, function (key, value) {

			if(city == value.ID_CIDADE)
			{
				option += '<option value="' + value.ID_CIDADE + '" ' + 'selected>' + value.NM_CIDADE + '</option>';	
			}else{


				option += '<option value="' + value.ID_CIDADE + '" ' + '>' + value.NM_CIDADE + '</option>';
			}
		})
		$("#cidade").html(option);
	}, 'json');
	
};



function getEstados(state,city){

	
	var option = '';

	$.get($("#urlestados").data('field-url'), null, function (json) {
		$.each(json, function (key, value) {
			if(state == value.ID_ESTADO ){
				option += '<option value="' + value.ID_ESTADO + '" selected>' + value.SG_ESTADO + '</option>';    
			}else{
				option += '<option value="' + value.ID_ESTADO + '" >' + value.SG_ESTADO + '</option>';
			}
		})
		$("#uf").html(option)
	}, 'json');
	
	
	cidade(state, city,  $("#urlcidades").data('field-url') );
}



function ufs () {
	var select = '';

	$.get($("#urlestados").data('field-url'), null, function (json) {
		select += ('<option selected disabled>Selecione um estado.</option>')
		$.each(json, function (key, value) {
			select += ('<option value="' + value.ID_ESTADO + '">' + value.SG_ESTADO + '</option>');
		})

		$("#uf").html(select)
	}, 'json');

};







