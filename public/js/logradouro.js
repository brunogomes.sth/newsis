 var logra;

 $( "#logradouro" ).autocomplete({
 	source: function(request, response){

 		var city = $("#cidade").val();

 		if(city === null)
 			return response([]);

 		var rest = $("#logradouro").data('field-url').split("{id}");
 		var url = rest[0] += city + rest[1] + "/"+request.term;

 		$.ajax({

 			'url'     : url,
 			'type'    : 'get',
 			'success' : function(data)
 			{
 				
 				if(data.length  === 0 || data[0].length === 0 )
 				{
 					
 					$("#auto").val('');
 					response([]); 

 				}else{

 					logra = data;
 					var aux = [];


 					$.each(data, function(k, v){   
 						var rua = v.NM_LOGRADOURO + " - "+v.DS_CEP; 
 						aux[k] =  rua;
 					});
 					response(aux);
 				}
 			}

 		});
 	},
 	minLength: 5,
 	select: function(event, ui){
 		crypt(ui);
 	}
 });

 function crypt(ui)
 {

 	$.each(logra, function(k, v){   

 			$("#cep").val( v.DS_CEP  );
 			$("#bairro").val( v.NM_BAIRRO );

			if(ui.item.label == ( v.NM_LOGRADOURO + " - " +v.DS_CEP) )
				$("#auto").val(v.ID_LOGRADOURO);
	});
 };
