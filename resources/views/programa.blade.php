@extends('admin_template')
@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Programas Cadastrados</h3>
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalPrograma">
                        <i class="fa fa-plus"></i> Novo
                    </button>
                </div>
                <!-- Modal para cadastro de programa -->
                <div class="modal fade" data-backdrop="static" id="ModalPrograma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <form method="post" action="{{route('programas')}}">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Cadastrar Programa</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Código:</label> 
                                                <input type="text" class="form-control input-sm " name="CD_PROGRAMA" value="" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Nome Do Programa:</label> 
                                                <input type="text" class="form-control input-sm " name="NM_PROGRAMA" value="">
                                                <input type="hidden" name="DT_CADASTRO" value="{{date('d/m/Y')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Plataforma :</label> 
                                                <select class="form-control input-sm" name="DS_PLATAFORM">
                                                    <option value="Windons">Windons</option>
                                                    <option value="Linux">Linux</option>
                                                    <option value="Mac">Mac</option>
                                                    <option value="Web">Web</option>
                                                    <option value="Android">Android</option>
                                                    <option value="Windons Phone">Windons Phone</option>
                                                    <option value="IOS">IOS</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success" value ="Salvar">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Fim Modal Programar-->
            </div><!-- /.box-header -->
            <!-- form start -->
            <!--<form role="form">-->
                <div class="box-body ">
                    <div class="table-responsive"> 
                        <table class="table table-bordered ">
                            <tbody>
                                <tr class="bg-light-blue">
                                    <th style="width: 10px">#</th>
                                    <th>Programa</th>
                                    <th class="text-center col-md-2">Versão</th>
                                    <th class="text-center col-md-2">Data da Versão</th>
                                    <th class="text-center col-md-2"> Ações </th>
                                </tr>
                                @foreach ($ListaProgramas as $ListaPrograma)
                                <tr>
                                    <td>{{$ListaPrograma->ID_PROGRAMA}}</td>
                                    <td>{{$ListaPrograma->NM_PROGRAMA}}</td>
                                    <td>{{$ListaPrograma->VERSAO}}</td>
                                    <td class="text-center">
                                        {{$ListaPrograma->DT_CADASTRO}}
                                    </td>
                                    <td class=" text-center">
                                        <a href="" class="btn btn-success btn-xs"> <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></a>
                                        <a href="" class="btn btn-primary btn-xs"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                        <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#ModalVersao{{$ListaPrograma->ID_PROGRAMA}}">
                                            <!--<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>-->
                                            <strong>Versões</strong>
                                        </button>
                                        <!-- Modal para cadastro de Versao -->
                                        <form action="{{route('versoes')}}" method="post">
                                            <div class="modal fade" data-backdrop="static" id="ModalVersao{{$ListaPrograma->ID_PROGRAMA}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Cadastrar Versão Para Programa</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- Tab panes -->
                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active" id="dados">
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>Programa:</label>
                                                                                <input type="text" class="form-control input-sm " name="" value="{{$ListaPrograma->NM_PROGRAMA}}" disabled>
                                                                                <input type="hidden" class="form-control input-sm " name="ID_PROGRAMA" value="{{$ListaPrograma->ID_PROGRAMA}}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>Versão:</label>
                                                                                <input type="text" class="form-control input-sm " name="DS_VERSION" value="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>Data da Versão:</label>
                                                                                <input type="date" class="form-control input-sm " name="DT_VERSION" value="{{date('d/m/Y')}}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label>Historico:</label>
                                                                                <textarea class="form-control input-sm " name="DS_HISTORICO"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="submit" class="btn btn-success" value ="Salvar">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- Fim Modal de versao-->   
                                    </td>
                                </tr>
                                @endforeach
                            </tbody></table>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div>
            <!--</form>-->
        </div><!-- /.box -->
    </div>
</div>
@endsection