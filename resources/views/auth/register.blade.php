@extends('admin_template')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Registrar novo Membro</h3>
                </div>
                <form method="POST" action="{{isset($usuario) ? route('user_atualizar',$usuario->id) : route('registrar_user')}}">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                @if (count($errors) > 0)
                                    <div class="alert alert-error alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                        </button>
                                        <ul class="list-unstyled">
                                            @foreach ($errors->all() as $error)
                                                <li><i class="fa fa-caret-right"> </i> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-3">
                                <label for="name">Nome:</label>
                                <input type="hidden" value="{{$usuario->ID_FUNCIONARIO or $id}}" name="ID_FUNCIONARIO">
                                <input type="text" class="form-control" name="name"
                                       value="{{$usuario->name or $nome}}"
                                       placeholder="Nome de Usuário" id="name">
                            </div>
                            <div class="form-group  col-md-3">
                                <label for="username">Nome do Usuário:</label>
                                <input type="text" class="form-control" name="username"
                                       value="{{$usuario->username or old('username')}}"
                                       placeholder="Nome de Usuário" id="username">
                            </div>
                            <div class="form-group  col-md-3">
                                <label for="email">E-mail:</label>
                                <input type="email" class="form-control" name="email"
                                       value="{{$usuario->email or $email}}" placeholder="Informe seu E-mail"
                                       id="email">

                            </div>
                            <div class="form-group col-md-3">
                                <label for="senha">Senha:</label>
                                <input type="password" class="form-control" name="password"
                                       placeholder="Digite um senha" id="senha"
                                        {{isset($usuario) ? 'disabled' : ''}}>

                            </div>
                            <div class="form-group col-md-3">
                                <label for="senha_c">Repita a Senha:</label>
                                <input type="password" class="form-control" name="password_confirmation"
                                       placeholder="Repita a Senha" id="senha_c"
                                        {{isset($usuario) ? 'disabled' : ''}}>
                            </div>
                        </div>
                        <!-- /.col -->
                        <!-- /.form-box -->
                    </div>
                    <div class="box-footer clearfix">
                        <a href="{{route('usuario')}}" class="btn btn-danger pull-right">Cancelar</a>
                        <input type="submit" class="btn btn-success pull-right" value="Salvar"
                               style="margin-right: 5px">
                    </div>
                    <!-- /.register-box -->
                </form>
            </div>
        </div>
    </div>
@endsection