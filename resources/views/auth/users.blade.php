@extends('admin_template')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Usuários Cadastrados</h3>
                    <div class="pull-right box-tools">
                        <a type="button" class="btn btn-primary btn-sm" href="{{route('funcionario')}}">Funcionários</a>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body ">
                        <div class="table-responsive">
                            <table id="usuario_table" class="table table-bordered table-hover">
                                <thead>
                                <tr class="bg-light-blue">
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 10px;">#</th>
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 10px;">Nome</th>
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 60px;">Usuário</th>
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 100px;">E-mail</th>
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 10px;">Ações
                                    </th>
                                </tr>
                                </thead>
                            </table>

                            {{--@push('scripts')--}}
                            <script>
                                $(function () {
                                    $('#usuario_table').DataTable({
                                        "autoWidth": true,
                                        "oLanguage": {
                                            "sProcessing": "Aguarde enquanto os dados são carregados ...",
                                            "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                                            "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                                            "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                                            "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                                            "sInfoFiltered": "",
                                            "sSearch": "Procurar",
                                            "oPaginate": {
                                                "sFirst": "Primeiro",
                                                "sPrevious": "Anterior",
                                                "sNext": "Próximo",
                                                "sLast": "Último"
                                            }
                                        },
                                        processing: true,
                                        serverSide: true,
                                        "ajax": {
                                            "url": "{!! route('usuario.data') !!}",
                                            "type": "POST"
                                        },
                                        columns: [
                                            {data: 'id', name: 'id'},
                                            {data: 'name', name: 'name'},
                                            {data: 'username', name: 'username'},
                                            {data: 'email', name: 'email'},
                                            {data: 'acoes', name: 'acoes', orderable: false, searchable: false}
                                        ]
                                    });
                                });
                            </script>
                            {{--@endpush--}}
                        </div>
                    </div><!-- /.box-body -->
                </form>
            </div><!-- /.box -->
        </div>
    </div>
@endsection