<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistel WEB | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->

    <link rel="stylesheet" href="{{ asset("/bower_components/AdminLte/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("/bower_components/AdminLte/dist/css/AdminLTE.min.css")}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset("/bower_components/AdminLte/plugins/iCheck/square/blue.css") }}">

    <style type="text/css">
        .sobra {
            -webkit-box-shadow: 10px 10px 30px 0px rgba(161, 161, 161, 1);
            -moz-box-shadow: 10px 10px 30px 0px rgba(161, 161, 161, 1);
            box-shadow: 10px 10px 30px 0px rgba(161, 161, 161, 1);
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition">
<div class="container">
    <div class="row" style="margin-top: 15%">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary sobra col-md-12">
                <div class="box-header">
                    <h2 class="text-center text-info"><strong>Sistel</strong>WEB</h2>
                </div>
                <div class="box-body">
                    <p class="text-center lead">Solicitar troca de senha</p>
                    <form method="POST" action="{{route('postEmail')}}">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-12 form-group has-feedback">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" placeholder="E-mail" name="email" id="email"
                                       value="{{ old('email') }}">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-8">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">
                                    Enviar E-mail
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-12">
                            @if (count($errors) > 0)
                                <div class="alert alert-error alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <ul class="list-unstyled">
                                        @foreach ($errors->all() as $error)
                                            <li><i class="fa fa-caret-right"> </i> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery 2.1.4 -->
<script src="{{ asset ("/bower_components/AdminLte/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ asset ("/bower_components/AdminLte/bootstrap/js/bootstrap.min.js") }}"></script>
<!-- iCheck -->
<script src="{{ asset ("/bower_components/AdminLte/plugins/iCheck/icheck.min.js")}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
