@extends('admin_template')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form method="post"
                      action="{{isset($dados) ? isset($editVers)? route('updateVersao',$dadosVersion->ID_VERSION) : route('atualizar',$dados->ID_PROGRAMA) : route('salvar')}}"
                      id="formulario">
                    {!! csrf_field() !!}
                    <div class="box-header with-border">
                        <h3 class="box-title">Cadastrar Programa</h3>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            <div class="col-md-12">
                                @if (count($errors) > 0)
                                    <div class="alert alert-error alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                        </button>
                                        <ul class="list-unstyled">
                                            @foreach ($errors->all() as $error)
                                                <li><i class="fa fa-caret-right"> </i> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="hidden" name="ID_PROGRAMA" class="programa"
                                           value="{{$dados->ID_PROGRAMA or ""}}">
                                    <input type="hidden" name="DT_CADASTRO" class="programa"
                                           value="{{$dados->DT_CADASTRO or date('Y-m-d')}}">

                                    <label for="nomePrograma">Nome Do Programa:</label>
                                    <input type="text" class="form-control input-sm programa text-uppercase" id="nomePrograma"
                                           value="{{$dados->NM_PROGRAMA or old('NM_PROGRAMA')}}" name="NM_PROGRAMA" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="plataforma">Plataforma :</label>

                                    {{Form::select('DS_PLATAFORM',
                                        ['' => 'Selecione...',
                                        'WINDOWS' => 'Windows',
                                        'LINUX' => 'Linux',
                                        'MAC' => 'Mac',
                                        'WEB' => 'Web',
                                        'ANDROID' => 'Android',
                                        'WINDOWS PHONE' => 'Windows Phone',
                                        'IOS' => 'IOS'],
                                        isset($dados->DS_PLATAFORM)? $dados->DS_PLATAFORM : null,['id'=>'plataforma', 'name'=>'DS_PLATAFORM' ,'class'=>'form-control input-sm text-uppercase  programa' ]
                                        )
                                        }}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="versao">Versão:</label>
                                    <input type="hidden" class="form-control input-sm versao" id="cdversao"
                                           name="ID_VERSION" value="{{$dadosVersion->ID_VERSION or ""}}">
                                    <input type="text" class="form-control input-sm text-uppercase versao" name="DS_VERSION"
                                           id="versao"
                                           value="{{$dadosVersion->DS_VERSION or  old('DS_VERSION')}}">
                                    {{--{{Form::text('email', $dados->DS_VERSION or  old('DS_VERSION'), ['class'=>'form-control input-sm programa' ])}}--}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="dtVersao">Data da Versão:</label>
                                    <input type="text" class="form-control input-sm versao data" name="DT_VERSION"
                                           id="dtVersao"
                                           value="{{$dadosVersion->DT_VERSION or old('DT_VERSION')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="historico">Histórico:</label>
                                <textarea class="form-control input-sm versao text-uppercase" name="DS_HISTORICO" id="historico"
                                >{{$dadosVersion->DS_HISTORICO or old('DS_HISTORICO')}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-success" value="Salvar">
                        <a href="{{route('programas')}}" class=" btn btn-danger">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset ("js/programa.js") }}"></script>
@endsection
