@extends('admin_template')
@section('content')
    <div class="row">
<!-- Main content -->
<section class="invoice">
    <!-- title row -->

        <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-desktop"></i> Detalhes do Programa.
                <small class="pull-right">Data: {{date('d/m/Y')}}</small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-2 invoice-col">
            <strong>Código</strong>
            <div class="lead">
                {{$programa->ID_PROGRAMA}}
            </div>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <strong>Nome Do Programa</strong>
            <div class="lead">
                {{$programa->NM_PROGRAMA}}
            </div>
        </div>
        <!-- /.col -->
        <div class="col-sm-3 invoice-col">
            <strong>Plataforma</strong>
            <div class="lead">
                {{$programa->DS_PLATAFORM}}
            </div>
        </div>
        <!-- /.col -->
         <div class="col-sm-3 invoice-col">
            <strong>Data de Cadastro</strong>
            <div class="lead">
                {{$programa->DT_CADASTRO}}
            </div>
        </div>
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12">
        <legend>Tabela de Versões</legend>
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="col-md-1">#</th>
                        <th class="text-left col-md-1">Versão</th>
                        <th class="text-left col-md-1">Lançamento</th>
                        <th class="text-left">Histórico</th>
                        <th class="text-right">Ações </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $versoes as $versao)
                    <tr>
                        <td>{{$versao->ID_VERSION}}</td>
                        <td class="">{{$versao->DS_VERSION}}</td>
                        <td class="text-left">{{$versao->DT_VERSION}}</td>
                        <td class="text-left">{{$versao->DS_HISTORICO}}</td>
                        <td class="text-right">
                            <a class="btn btn-primary btn-xs editar"
                               href="{{route('editVersao',$versao->ID_VERSION)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <button type="button" class="btn btn-danger btn-xs" 
                                    data-excluir="Versão" data-id="{{$versao->ID_VERSION}}" data-nome="{{$versao->DS_VERSION}}"
                                    data-toggle="modal" data-target="#ModalExcluir" data-url="{{url('/')}}">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <a class="btn btn-success pull-right" href="{{route('programas')}}"><i class="fa fa-sign-in"></i> Voltar
            </a>
            <button type="button" class="btn btn-danger pull-right" style="margin-right: 10px;"
                    data-excluir="Programa" data-id="{{$programa->ID_PROGRAMA}}" data-nome="{{$programa->NM_PROGRAMA}}"
                    data-toggle="modal" data-target="#ModalExcluir" data-url="{{url('/')}}">
                <i class="fa fa-trash-o"></i> Deletar
            </button>
            <a class="btn btn-primary pull-right editar" style="margin-right: 5px;"
               href="{{route('editar',$programa->ID_PROGRAMA)}}">
                <i class="fa fa-edit"></i> Editar
            </a>
        </div>
    </div>
</section>
<!--Modal para adicionar versão-->
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal modal-info fade" id="ModalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titulo"></h4>
            </div>
            <div class="modal-body">

                <p class="modal-conteudo"></p>
                <p class="modal-obs"></p>
            </div>
            <div class="modal-footer">
                <a href="" id="confirma" class="btn btn-outline">Confirmar</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
</div>
<script src="{{ asset ("js/programa.js") }}"></script>
@endsection