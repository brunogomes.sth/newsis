@extends('admin_template')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Programas Cadastrados</h3>
                    <div class="pull-right box-tools">
                        <a type="button" class="btn btn-primary btn-sm" href="{{route('p_adicionar')}}"><i
                                    class="fa fa-plus"></i> Novo</a>
                    </div>
                </div><!-- /.box-header -->
                <!--Modal para adicionar versão-->
                <div class="modal fade" data-backdrop="static" id="ModalVersao" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Cadastrar Versão Para Programa</h4>
                            </div>
                            <form action="{{route('salvarVersao')}}" method="post">
                                <div class="modal-body">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="dados">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    {!! csrf_field() !!}
                                                    <div class="form-group">
                                                        <label for="programa">Programa:</label>
                                                        <input type="text"
                                                               class="form-control input-sm text-uppercase nome"
                                                               disabled
                                                               id="programa" >
                                                        <input type="hidden" class="form-control input-sm id"
                                                               name="ID_PROGRAMA">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="versao">Versão:</label>
                                                        <input type="text" class="form-control input-sm text-uppercase "
                                                               name="DS_VERSION"
                                                               id="versao"
                                                               value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="dtVersao">Data da Versão:</label>
                                                        <input type="text" class="form-control input-sm data"
                                                               name="DT_VERSION"
                                                               id="dtVersao"
                                                               value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="historico">Histórico:</label>
                                                        <textarea class="form-control input-sm text-uppercase "
                                                                  name="DS_HISTORICO" id="historico"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success" value="Salvar">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- fim do modal -->

                <!-- form start -->
                <form role="form">
                    <div class="box-body ">
                        <div class="table-responsive">
                            <table id="programas_table" class="table table-bordered table-hover programas_table">
                                <thead>
                                <tr class="bg-light-blue">
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 10px;">#
                                    </th>

                                    <th>Programa</th>
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 60px;">Plataforma
                                    </th> <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 60px;">Versão
                                    </th>
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 100px;">Data da Versão
                                    </th>
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action"
                                        style="width: 100px;">Ações
                                    </th>
                                </tr>
                                </thead>
                            </table>

                            {{--@push('scripts')--}}
                            <script>
                                $(function () {
                                    $('#programas_table').DataTable({
                                        "autoWidth": true,
                                        "oLanguage": {
                                            "sProcessing": "Aguarde enquanto os dados são carregados ...",
                                            "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                                            "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                                            "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                                            "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                                            "sInfoFiltered": "",
                                            "sSearch": "Procurar",
                                            "oPaginate": {
                                                "sFirst": "Primeiro",
                                                "sPrevious": "Anterior",
                                                "sNext": "Próximo",
                                                "sLast": "Último"
                                            }
                                        },
                                        processing: true,
                                        serverSide: true,
                                        "ajax": {
                                            "url": "{!! route('programa.data') !!}",
                                            "type": "POST"
                                        },
                                        columns: [
                                            {data: 'ID_PROGRAMA', name: 'ID_PROGRAMA'},
                                            {data: 'NM_PROGRAMA', name: 'NM_PROGRAMA'},
                                            {data: 'DS_PLATAFORM', name: 'DS_PLATAFORM', className: 'text-center'},
                                            {data: 'VERSAO', name: 'VERSAO', className: 'text-center'},
                                            {data: 'DT_VERSAO', name: 'DT_VERSAO', className: 'text-center'},
                                            {data: 'acoes', name: 'acoes', orderable: false, searchable: false}
                                        ]
                                    });
                                });
                            </script>
                            {{--@endpush--}}
                        </div>
                    </div><!-- /.box-body -->
                </form>
            </div><!-- /.box -->
        </div>
    </div>
    <script src="{{ asset ("js/programa.js") }}"></script>
@endsection