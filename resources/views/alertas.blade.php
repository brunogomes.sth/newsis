@if (Session::has('flash_message_success'))
    <script>

        window.onload = function(){
            PNotify.prototype.options.styling = "bootstrap3";
            new PNotify({
                title: 'Sucesso!',
                text: '{{ Session::get('flash_message_success') }}',
                type: 'success',
            });
        };
    </script>
@endif
@if (Session::has('flash_message_info'))
<script>
    window.onload = function(){
        PNotify.prototype.options.styling = "bootstrap3";
        new PNotify({
            title: 'Informação',
            text: '{{ Session::get('flash_message_info') }}',
            type: 'info',
        });
    };
</script>
@endif
@if (Session::has('flash_message_error'))
    <script>
        window.onload = function(){
            PNotify.prototype.options.styling = "bootstrap3";
            new PNotify({
                title: 'Erro!',
                text: '{{ Session::get('flash_message_error') }}',
                type: 'error',
            });

        };
    </script>
@endif
