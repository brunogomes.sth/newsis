<div class="col-md-12 table-responsive ">
        <table id="products-table" class="table table-bordered table-striped ">
            <thead>
                <tr class="bg-light-blue">
                    <th>#</th>
                    <th>Programa</th>
                    <th>Versão</th>
                    <th>Plataforma</th>
                    <th class="col-md-1 text-center">Excluir</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cliPrg as $cliPr)

                <tr>
                    <td>{{$cliPr->ID_PROGRAMA_PESSOA}}</td>
                    <td>{{$cliPr->NM_PROGRAMA}}</td>
                    <td>{{$cliPr->DS_VERSION}}</td>
                    <td>{{$cliPr->DS_PLATAFORM}}</td>
                    <td class="text-center"><a type="button"
                     href="{{route('programaClienteRemove', $cliPr->ID_PROGRAMA_PESSOA)}}"
                     class="btn btn-danger btn-xs"><span
                     class="glyphicon glyphicon-trash" aria-hidden="true"></a>
                 </td>

             </tr>

             @endforeach
         </tbody>
     </table>
 </div>