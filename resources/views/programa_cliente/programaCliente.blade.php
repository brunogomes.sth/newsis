@extends('admin_template')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form method="post" action="{{route('programaClienteAdd')}}" id="formulario">
                    {!! csrf_field() !!}
                    <div class="box-header with-border">
                        <h3 class="box-title">Programas utilizados pelo cliente: {{$pessoas->NM_PESSOA}}</h3>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            {{Form::hidden('ID_PESSOA', $pessoas->ID_PESSOA)}}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Programa:</label>
                                            <select id="programas" name="" class="form-control  input-sm" required="">
                                                <option value="">Selecione um Programa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Versão:</label>

                                            <select id="versao" name="ID_VERSION" class="form-control input-sm"
                                                    required=""></select>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label style="color: #fff">&nbsp; &nbsp;</label>
                                            <br>
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                                <strong>Adcionar</strong>
                                            </button>
                                            <a class="btn btn-success btn-sm" href="{{ route('showCliente', $pessoas->ID_PESSOA)}}"> <span class="glyphicon glyphicon-list-alt"></span>  Ver Cliente</a>
                                        </div>

                                    </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 table-responsive ">
                                <table id="products-table" class="table table-bordered table-striped ">
                                    <thead>
                                    <tr class="bg-light-blue">
                                        <th>#</th>
                                        <th>Programa</th>
                                        <th>Versão</th>
                                        <th>Plataforma</th>
                                        <th class="col-md-1 text-center">Excluir</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($cliPrg as $cliPr)

                                        <tr>
                                            <td>{{$cliPr->ID_PROGRAMA_PESSOA}}</td>
                                            <td>{{$cliPr->NM_PROGRAMA}}</td>
                                            <td>{{$cliPr->DS_VERSION}}</td>
                                            <td>{{$cliPr->DS_PLATAFORM}}</td>
                                            <td class="text-center"><a type="button"
                                                                       href="{{route('programaClienteRemove', $cliPr->ID_PROGRAMA_PESSOA)}}"
                                                                       class="btn btn-danger btn-xs"><span
                                                            class="glyphicon glyphicon-trash" aria-hidden="true"></a>
                                            </td>

                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset ("js/programa.js") }}"></script>

    <script>

        $(function(){

            var select = '<option disabled selected>Selecione um Programa</option>';
            $.get("{{url('/getPrograma/')}}", null, function (json) {
                $.each(json, function (key, value) {
                    select += ('<option value="' + value.ID_PROGRAMA + '">'+ value.NM_PROGRAMA+'</option>');
                });

                $("#programas").html(select);
                $("#programas").change( function(){  versao( $("#programas").val() )  }  );  
            }, 'json');

        });


        function versao(options) {

            var select = '<option disabled selected>Selecione uma versão</option>';
            $.get("{{url('/getVersion/')}}/" + options, null, function (json) {
                $.each(json, function (key, value) {
                    select += ('<option value="' + value.ID_VERSION + '" > ' + value.DS_VERSION + '</option>');
                })
                $("#versao").html(select);
            }, 'json');

        };

    </script>

@endsection
