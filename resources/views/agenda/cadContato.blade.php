@extends('admin_template')

@section('content')

    <div class="row">
<form action=" {{ isset($contatos->ID_CONTATO) ? route('updateContato', $contatos->ID_CONTATO) : route('salvarContato') }}" method="post">

        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Novo Contato</h3>
                </div>

                <div class="box-body ">
                    <div class="row">
                        <div class="col-md-12">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                                    <ul class="list-unstyled">
                                        @foreach ($errors->all() as $error)
                                            <li> <i class="fa fa-caret-right "> </i> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Código:</label>
                                <input type="text" class="form-control input-sm programa"
                                       id="cdprograma" name="CD_CONTATO" value="{{$contatos->ID_CONTATO or ""}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nome:</label>
                                <input type="text" class="form-control input-sm programa"
                                       value="{{$contatos->NM_CONTATO or old('NM_CONTATO')}}" name="NM_CONTATO" required maxlength="50" >
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Telefone 1:</label>
                                <input type="text" class="form-control input-sm programa mask tel"
                                       value="{{$contatos->DS_TEL_01 or  old('DS_TEL_01')}}" name="DS_TEL_01" >
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Telefone 2:</label>
                                <input type="text" class="form-control input-sm programa mask tel"
                                       value="{{$contatos->DS_TEL_02 or  old('DS_TEL_02')}}" name="DS_TEL_02">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Celular 1:</label>
                                <input type="text" class="form-control input-sm programa mask cel"
                                       value="{{$contatos->DS_CEL_01 or  old('DS_CEL_01')}}" name="DS_CEL_01">
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Celular 2:</label>
                                <input type="text" class="form-control input-sm programa mask cel"
                                       value="{{$contatos->DS_CEL_02 or  old('DS_CEL_02')}}" name="DS_CEL_02" >
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>E-mail:</label>
                                <input type="text" class="form-control input-sm programa"
                                       value="{{$contatos->DS_EMAIL or  old('DS_EMAIL')}}" name="DS_EMAIL" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <input type="submit" class="btn btn-success" value="Salvar">
                    <a href="{{route('agenda')}}" class=" btn btn-danger">Cancelar</a>
                </div>
            </div>
        </div>
        </form>

        <!-- Fim Modal -->

    </div><!-- /.box -->

    <script type="text/javascript">
        //$('#uf').ufs();
        $("#uf").focus(function () {
            $('#uf').ufs();
        });

        $(document).ready(function(){
            $('.data').mask('00/00/0000');
        });

        $(".mask").focus(function () {
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cep').mask('00000-000');
            $('.cel').mask('(00) 00000-0000');
            $('.tel').mask('(00) 0000-0000');
        });
        $(".mask").blur(function () {
            $('.cpf').unmask();
            $('.cep').unmask();
            $('.cel').unmask();
            $('.tel').unmask();
        });

    </script>


@endsection
