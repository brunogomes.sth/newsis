@extends('admin_template')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Agenda Telefonica</h3>
                    <div class="pull-right box-tools">
                        <a type="button" class="btn btn-primary btn-sm" href="{{route('novoContato')}}"><i
                                    class="fa fa-plus"></i> Novo</a>
                    </div>
                    <!-- Modal -->
                    <!-- Fim Modal -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body ">


                        <div class="table-responsive">
                            <table id="agenda_table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr class="bg-light-blue">
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th class="text-center" rowspan="1" colspan="1"  style="width: 80px;">Telefone 1</th>
                                    <th class="text-center" rowspan="1" colspan="1"  style="width: 80px;">Telefone 2</th>
                                    <th class="text-center" rowspan="1" colspan="1"  style="width: 80px;">Celular 1</th>
                                    <th class="text-center" rowspan="1" colspan="1"  style="width: 80px;">Celular 2</th>
                                    <th class="text-center" rowspan="1" colspan="1"  style="width: 80px;">Tipo</th>
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action" style="width: 56px;">Ações</th>
                                </tr>
                                </thead>
                            </table>
                            {{--@push('scripts')--}}
                            <script>
                                $(function () {
                                    $('#agenda_table').DataTable({
//                                        "paging": true,
//                                        "lengthChange": false,
//                                        "searching": false,
//                                        "ordering": true,
//                                        "info": true,
                                      "autoWidth": true,
                                        "oLanguage": {
                                            "sProcessing": "Aguarde enquanto os dados são carregados ...",
                                            "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                                            "sZeroRecords": "Nenhum registro encontrado",
                                            "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                                            "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                                            "sInfoFiltered": "",
                                            "sSearch": "Procurar",
                                            "oPaginate": {
                                                "sFirst":    "Primeiro",
                                                "sPrevious": "Anterior",
                                                "sNext":     "Próximo",
                                                "sLast":     "Último"
                                            }
                                        },
                                        processing: true,
                                        serverSide: true,
                                        "ajax": {
                                            "url": "{!! route('agenda.data') !!}",
                                            "type": "POST"
                                        },
                                        columns: [
                                            { data: 'ID_CONTATO', name: 'ID_CONTATO'},
                                            { data: 'NM_CONTATO', name: 'NM_CONTATO' },
                                            { data: 'DS_TEL_01', name: 'DS_TEL_01', className: 'text-center'},
                                            { data: 'DS_TEL_02', name: 'DS_TEL_02', className: 'text-center'},
                                            { data: 'DS_CEL_01', name: 'DS_CEL_01' , className: 'text-center'},
                                            { data: 'DS_CEL_02', name: 'DS_CEL_02' , className: 'text-center'},
                                            { data: 'TIPO_CONTATO', name: 'TIPO_CONTATO', orderable: false, searchable: false, className: 'text-center'},
                                            { data: 'acoes', name: 'acoes', orderable: false, searchable: false }
                                        ]
                                    });
                                });
                            </script>
                            <script type="text/javascript">
                                //$('#uf').ufs();
                                $("#uf").focus(function () {
                                    $('#uf').ufs();
                                });
                                $(document).ready(function(){
                                $('.data').mask('00/00/0000');
                                $('.cpf').mask('000.000.000-00', {reverse: true});
                                $('.cep').mask('00000-000');
                                $('.cel').mask('(00) 00000-0000');
                                $('.tel').mask('(00) 0000-0000');
                                });
                            </script>
                            {{--@endpush--}}
                        </div>
                    </div><!-- /.box-body -->
                </form>
            </div><!-- /.box -->
        </div>
    </div>
@endsection