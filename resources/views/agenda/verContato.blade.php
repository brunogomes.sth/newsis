@extends('admin_template')
@section('content')
        <!-- Main content -->
<div class="row">
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-desktop"></i> Detalhes do Contato.
                    <small class="pull-right">Date: {{date('d/m/Y')}}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <!-- Dados do identificação funcionario-->
        <div class="row invoice-info">
            <div class="col-xs-12">

                <legend>Contato</legend>
                <div class="col-md-2 invoice-col">
                    <strong>Código:</strong>
                    <div class="lead">
                        {{$contatos->ID_CONTATO}}
                    </div>
                </div>
                <div class="col-md-4 invoice-col">
                    <strong>Nome:</strong>
                    <div class="lead">
                        {{$contatos->NM_CONTATO}}
                    </div>
                </div>

                <div class="col-md-3 invoice-col">
                    <strong class="text-center">Telefone 1:</strong>
                    <div class="lead">
                        {{mask($contatos->DS_TEL_01, "(##) ####-####")}}
                    </div>
                </div>
                <div class="col-md-3 invoice-col">
                    <strong class="text-center">Telefone 1:</strong>
                    <div class="lead">
                        {{mask($contatos->DS_TEL_02, "(##) ####-####")}}
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <div class="col-md-12">
                <div class="col-md-3 invoice-col">
                    <strong>Celular 1:</strong>
                    <div class="lead">
                        {{mask($contatos->DS_CEL_01,'(##) #####-####')}}
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-3 invoice-col">
                    <strong>Celular 2:</strong>
                    <div class="lead">
                        {{mask($contatos->DS_CEL_02,'(##) #####-####')}}
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-4 invoice-col">
                    <strong>E-mail:</strong>
                    <div class="lead">
                        {{$contatos->DS_EMAIL}}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- /.row -->
        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <hr>
                <a class="btn btn-success pull-right" href="{{route('agenda')}}"><i class="fa fa-sign-in"></i>
                    Voltar</a>
                <a class="btn btn-primary pull-right editar" style="margin-right: 5px;"
                   href="{{route('editaContato', $contatos->ID_CONTATO)}}">
                    <i class="fa fa-edit"></i> Editar
                </a>
            </div>
        </div>
    </section>
</div>
@endsection