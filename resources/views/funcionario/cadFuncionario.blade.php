@extends('admin_template')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <form action="{{ isset($pessoa->ID_PESSOA) ? route('f_atualizar' , $pessoa->ID_PESSOA ) : route('f_salvar') }}"
                      method="POST">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cadastrado de Funcionário</h3>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            <div class="col-md-12">
                                @if (count($errors) > 0)
                                    <div class="alert alert-error alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                        </button>
                                        <ul class="list-unstyled">
                                            @foreach ($errors->all() as $error)
                                                <li><i class="fa fa-caret-right"> </i> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="nav-tabs-custom">
                            <!-- Barra de navegação-->
                            <ul class="nav nav-tabs" role="tablist">
                                <li id="" role="presentation" class="active">
                                    <a href="#dados" aria-controls="dados" role="tab" data-toggle="tab">Dados</a></li>
                                <li role="presentation">
                                    <a href="#endereco" aria-controls="endereco" role="tab"
                                       data-toggle="tab">Endereço</a></li>
                                <li role="presentation">
                                    <a href="#contato" aria-controls="contato" role="tab" data-toggle="tab">Contatos</a>
                                </li>
                                <li role="presentation">
                                    <a href="#funcionario" aria-controls="funcionario" role="tab" data-toggle="tab">Funcionário</a>
                                </li>
                                <li role="presentation">
                                    <a href="#outras" aria-controls="outras" role="tab" data-toggle="tab">Outras
                                        Informações</a></li>
                            </ul>

                            <!-- paines  -->
                            <div class="tab-content">
                                <!-- Peimeiro painel (DADOS PESSOAIS)-->
                                <div role="tabpanel" class="tab-pane active" id="dados">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="codigo">Código:</label>
                                                <input type="hidden" name="FL_FUNCIONARIO" value="1">
                                                <input type="hidden" name="TP_PESSOA" value="1">
                                                <input type="text" class="form-control input-sm" id="codigo"
                                                       value="{{$pessoa->ID_PESSOA or '' }}" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="nome">Nome:</label>
                                                <input type="text" class="form-control input-sm text-uppercase soLetras"
                                                       name="NM_PESSOA" id="nome"
                                                       value="{{$pessoa->NM_PESSOA or old('NM_PESSOA') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="data">Data de Nasc.:</label>
                                                <input type="text" class="form-control input-sm data" name="DT_NASC"
                                                       id="data"
                                                       value="{{$pessoa->fisica->DT_NASC or old('DT_NASC') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="rg">RG:</label>
                                                <input type="text" class="form-control input-sm mask rg" name="CD_RG"
                                                       id="rg"
                                                       value="{{$pessoa->fisica->CD_RG or old('CD_RG') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="cpf">CPF:</label>
                                                <input type="text" class="form-control input-sm mask cpf" name="CD_CPF"
                                                       id="cpf"
                                                       value="{{$pessoa->fisica->CD_CPF or old('CD_CPF') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Segundo painel (ENDEREÇO)-->
                                <div role="tabpanel" class="tab-pane" id="endereco">
                                    <div class="row">

                                        <!-- JAVASCRIP PREENCHER CIDADE/ESTADO | CASO CAIA EM VALIDATIONS TBM -->
                                        <input type="hidden" id="state"
                                               data-field-id="{{ $pessoas->logradouro->bairro->cidade->ID_ESTADO or old('UF') }}">
                                        <input type="hidden" id="city"
                                               data-field-id="{{ $pessoas->logradouro->bairro->cidade->ID_CIDADE or old('ID_CIDADE') }}">
                                        <input type="hidden" id="urlcidades" data-field-url="{{ url('/cidades/') }}">
                                        <input type="hidden" id="urlestados" data-field-url="{{ url('/ufs/') }}">
                                        <script src="{{ asset ("/js/getEstados.js") }}"></script>
                                        <script src="{{ asset ("/js/jquery-ui.js") }}"></script>
                                        <link rel="stylesheet" type="text/css"
                                              href="{{ asset ("/css/jquery-ui.css") }}">

                                        <script type="text/javascript">

                                            var state = $("#state").data('field-id');
                                            var city = $("#city").data('field-id');

                                            if (state > 0)
                                                getEstados(state, city);
                                            else
                                                ufs();
                                        </script>

                                        <!--FIM CHAMADAS JAVASCRIPTS-->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>UF:</label>
                                                <select id="uf" class="form-control input-sm" name="UF" default="CE">
                                                    @if(isset($cidades))
                                                        @foreach($cidades as $cidade)
                                                            <option value="{{$cidade->ID_ESTADO}}">{{$cidade->SG_ESTADO}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Cidade:</label>
                                                <select class="form-control input-sm" name="ID_CIDADE" id="cidade">
                                                    @if(isset($cidades))
                                                        @foreach($cidades as $cidade)
                                                            <option value="{{$cidade->ID_CIDADE}}">{{$cidade->NM_CIDADE}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Endereço:</label>
                                                <input type="text" id="logradouro"
                                                       data-field-url="{{ url('logradouro/cidade/{id}/logradouro/') }}"
                                                       class="form-control input-sm" name="NM_LOGRADOURO"
                                                       value="{{$pessoas->logradouro->NM_LOGRADOURO or old('NM_LOGRADOURO') }}">

                                                <input type="hidden" name="auto" id="auto"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Bairro:</label>
                                                <input type="text" id="bairro" class="form-control input-sm"
                                                       name="NM_BAIRRO"
                                                       value="{{$pessoas->logradouro->bairro->NM_BAIRRO or old('NM_BAIRRO') }}">
                                            </div>
                                        </div>

                                        <script src="{{ asset ("/js/logradouro.js") }}"></script>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="cep">CEP:</label>
                                                <input type="text" class="form-control input-sm mask cep" name="DS_CEP"
                                                       id="cep"
                                                       value="{{$pessoas->logradouro->DS_CEP or old('DS_CEP') }}">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Nº:</label>
                                                <input type="text" class="form-control input-sm" name="NN_NUMERO"
                                                       value="{{$pessoas->NN_NUMERO or old('NN_NUMERO') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Complemento:</label>
                                                <input type="text" class="form-control input-sm" name="DS_COMPLEMENTO"
                                                       value="{{$pessoas->DS_COMPLEMENTO or old('DS_COMPLEMENTO') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Terceiro painel (CONTATO)-->
                                <div role="tabpanel" class="tab-pane" id="contato">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="tel1">TEL1:</label>
                                                <input type="text" class="form-control input-sm mask tel"
                                                       name="DS_TEL_01" id="tel1"
                                                       value="{{$pessoa->contato->DS_TEL_01 or old('DS_TEL_01') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="tel2">TEL2:</label>
                                                <input type="text" class="form-control input-sm mask tel"
                                                       name="DS_TEL_02" id="tel2"
                                                       value="{{$pessoa->contato->DS_TEL_02 or old('DS_TEL_02') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="cel1">CEL1:</label>
                                                <input type="text" class="form-control input-sm mask cel"
                                                       name="DS_CEL_01" id="cel1"
                                                       value="{{$pessoa->contato->DS_CEL_01 or old('DS_CEL_01') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="cel2">CEL2:</label>
                                                <input type="text" class="form-control input-sm mask cel"
                                                       name="DS_CEL_02" id="cel2"
                                                       value="{{$pessoa->contato->DS_CEL_02 or old('DS_CEL_02') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="email">E-mail:</label>
                                                <input type="text" class="form-control input-sm text-uppercase"
                                                       name="DS_EMAIL"
                                                       id="email"
                                                       value="{{$pessoa->contato->DS_EMAIL or old('DS_EMAIL') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Quarto painel (FUNCIONARIO)-->
                                <div role="tabpanel" class="tab-pane" id="funcionario">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="comissao">Comissão</label>
                                                <input type="text" class="form-control input-sm soNumero" min="0"
                                                       name="COMISSAO" id="comissao"
                                                       value="{{$pessoa->funcionario->COMISSAO or old('COMISSAO')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="cargo">Cargo:</label>
                                                <select class="form-control input-sm" name="ID_CARGO" id="cargo">
                                                    <option value="">Selecione...</option>
                                                    @foreach($cargos as $cargo)
                                                        @if($cargo->ID_CARGO == (isset($pessoa) ? $pessoa->funcionario->ID_CARGO : old('ID_CARGO') ))
                                                            <option value="{{$cargo->ID_CARGO}}"
                                                                    selected>{{$cargo->NM_CARGO}}</option>
                                                        @else
                                                            <option value="{{$cargo->ID_CARGO}}">{{$cargo->NM_CARGO}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="setor">Setor:</label>
                                                <select class="form-control input-sm" name="ID_SETOR" id="setor">
                                                    <option value="">Selecione...</option>
                                                    @foreach($setores as $setor)
                                                        @if( (isset($pessoa) ? $pessoa->funcionario->ID_SETOR : old('ID_SETOR') ) == $setor->ID_SETOR)
                                                            <option value="{{$setor->ID_SETOR}}"
                                                                    selected>{{$setor->NM_SETOR}}</option>
                                                        @else
                                                            <option value="{{$setor->ID_SETOR}}">{{$setor->NM_SETOR}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="apelido">Apelido</label>
                                                <input type="text" class="form-control input-sm text-uppercase soLetras"
                                                       name="APELIDO" id="apelido"
                                                       value="{{$pessoa->fisica->APELIDO or old('APELIDO')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Ativo</label><br>
                                                <label class="radio-inline">
                                                    {{ Form::radio('FL_ATIVO', '1', isset($pessoa) && $pessoa->FL_ATIVO == '1' || (old('FL_ATIVO') == '1'), array('id'=>'FL_ATIVO_SIM'))}}
                                                    Sim
                                                </label>
                                                <label class="radio-inline">
                                                    {{ Form::radio('FL_ATIVO', '0',isset($pessoa) && $pessoa->FL_ATIVO == '0' ||  (old('FL_ATIVO') == '0'), array('id'=>'FL_ATIVO_NAO'))}}
                                                    Não
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Quinto painel (OBSERVEÇÃO)-->
                                <div role="tabpanel" class="tab-pane" id="outras">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="observasao">Obs. (Opcional):</label>
                                                <textarea class="form-control input-sm text-uppercase" name="DS_OBS"
                                                          rows="6"
                                                          id="observasao"
                                                >{{$pessoa->DS_OBS or old('DS_OBS')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer clearfix">
                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
                        <a class="btn btn-danger" href="{{route('funcionario')}}"><i class="fa fa-sign-in"></i> Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- /.box -->
    <script>
        $("#uf").change(function () {
            cidade($("#uf").val(), null, $("#urlcidades").data('field-url'));
        });
    </script>

    <script type="text/javascript">
        //$('#uf').ufs();
        $("#uf").focus(function () {
            $('#uf').ufs();
        });
        $(document).ready(function () {
            $('.data').mask('00/00/0000');
            $(".mask").on({
                focus: function () {
                    $('.cpf').mask('000.000.000-00', {reverse: true});
                    $('.cep').mask('00000-000');
                    $('.cel').mask('(00) 00000-0000');
                    $('.tel').mask('(00) 0000-0000');
                    $('.rg').mask('000000000000000');
                },
                blur: function () {
                    $('.cpf').unmask();
                    $('.cep').unmask();
                    $('.cel').unmask();
                    $('.tel').unmask();
                    $('.rg').unmask();
                }
            });

            $(".soLetras").keyup(function () {
                var $this = $(this); //armazeno o ponteiro em uma variavel
                var valor = $this.val().replace(/[^a-zA-Z à-úÀ-Ú]+/g, '');
                $this.val(valor);
            });

            $(".soNumero").keyup(function () {
                var $this = $(this); //armazeno o ponteiro em uma variavel
                var valor = $this.val().replace(/[^0-9,.]+/g, '');
                $this.val(valor);
            });
        });
    </script>
@endsection