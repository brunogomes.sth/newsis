@extends('admin_template')
@section('content')
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-desktop"></i> Detalhes do Funcionário.
                    <small class="pull-right">Date: {{date('d/m/Y')}}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <!-- Dados do identificação funcionario-->
        <div class="row invoice-info">
            <legend>Funcionário</legend>
            <div class="col-sm-2 invoice-col">
                <strong>Código:</strong>
                <div class="lead">
                    {{$pessoa->funcionario->ID_FUNCIONARIO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-5 invoice-col">
                <strong>Nome Do Funcionário:</strong>
                <div class="lead">
                    {{$pessoa->NM_PESSOA}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-2 invoice-col">
                <strong>Status:</strong>
                <div class="lead">
                    @if ($pessoa->FL_ATIVO > 0)
                        {{'Ativo'}}
                    @else
                        {{'Inativo'}}
                    @endif
                </div>
            </div>
            <div class="col-sm-2 invoice-col">
                <strong>Data de Cadastro:</strong>
                <div class="lead">
                    {{$pessoa->created_at}}
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- Dados função do funcionario-->
        <div class="row invoice-info">
            <div class="col-sm-2 invoice-col">
                <strong>Cargo:</strong>
                <div class="lead">
                    {{$pessoa->funcionario->cargo->NM_CARGO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>Setor:</strong>
                <div class="lead">
                    {{$pessoa->funcionario->setor->NM_SETOR}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-2  col-sm-offset-2 invoice-col">
                <strong>Comissão:</strong>
                <div class="lead">
                    {{$pessoa->funcionario->COMISSAO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-2 invoice-col">
                <strong>Apelido:</strong>
                <div class="lead">
                    {{$pessoa->fisica->APELIDO}}
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- Dados Contato do funcionario-->
        <div class="row invoice-info">
            <legend>Contatos</legend>
            <div class="col-sm-4 invoice-col">
                <strong class="text-center">Telefones:</strong>
                <div class="lead">
                    {{$pessoa->contato->DS_TEL_01}} / {{$pessoa->contato->DS_TEL_02}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>Celulares:</strong>
                <div class="lead">
                    {{$pessoa->contato->DS_CEL_01}} / {{$pessoa->contato->DS_CEL_02}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>E-mail:</strong>
                <div class="lead">
                    {{$pessoa->contato->DS_EMAIL}}
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- Dados Endereco do funcionario-->
        <div class="row invoice-info">
            <legend>Endereço</legend>
            <div class="col-sm-3 invoice-col">
                <strong class="text-center">CEP:</strong>
                <div class="lead">
                    {{$pessoa->logradouro->DS_CEP}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>UF:</strong>
                <div class="lead">
                    {{$pessoa->logradouro->bairro->cidade->estado->SG_ESTADO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>Cidade:</strong>
                <div class="lead">
                    {{$pessoa->logradouro->bairro->cidade->NM_CIDADE}}
                </div>
            </div>
            <div class="col-sm-3 invoice-col">
                <strong>Bairro:</strong>
                <div class="lead">
                    {{$pessoa->logradouro->bairro->NM_BAIRRO}}
                </div>
            </div>
        </div>
        <!-- /.col -->
        <!-- Dados Enreco do funcionario-->
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <strong class="text-center">Logradouro:</strong>
                <div class="lead">
                    {{$pessoa->logradouro->NM_LOGRADOURO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>Número:</strong>
                <div class="lead">
                    {{$pessoa->NN_NUMERO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>Complemento:</strong>
                <div class="lead">
                    {{$pessoa->DS_COMPLEMENTO}}
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <hr>
                <a class="btn btn-info" href="
                    {{route('user_adicionar',['id'=>$pessoa->funcionario->ID_FUNCIONARIO,'nome'=>$pessoa->NM_PESSOA,'email'=>$pessoa->contato->DS_EMAIL])}}">
                    <i class="fa fa-user-plus"></i> Adicionar Usuário
                </a>
                <a class="btn btn-danger pull-right" href="{{route('funcionario')}}"><i class="fa fa-sign-in"></i>
                    Voltar
                </a>
                <a class="btn btn-primary pull-right editar" style="margin-right: 5px;"
                   href="{{route('f_editar',$pessoa->ID_PESSOA)}}">
                    <i class="fa fa-edit"></i> Editar
                </a>
            </div>
        </div>
    </section>
@endsection