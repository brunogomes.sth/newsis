@extends('admin_template')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Funcionários Cadastrados</h3>
                    <div class="pull-right box-tools">
                        <a type="button" class="btn btn-primary btn-sm" href="{{route('f_adicionar')}}"><i
                                    class="fa fa-plus"></i> Novo</a>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="funcionario_table" class="table table-bordered table-hover">
                                <thead>
                                <tr class="bg-light-blue-gradient">
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Telefone</th>
                                    <th>Celular</th>
                                    <th class="text-center" rowspan="1" colspan="1" aria-label="Action" style="width: 60px;">Ações</th>
                                </tr>
                                </thead>
                            </table>
                            {{--@push('scripts')--}}
                            <script>
                                $(function () {
                                    $('#funcionario_table').DataTable({
                                        "autoWidth": true,
                                        "oLanguage": {
                                            "sProcessing": "Aguarde enquanto os dados são carregados ...",
                                            "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                                            "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                                            "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                                            "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                                            "sInfoFiltered": "",
                                            "sSearch": "Procurar",
                                            "oPaginate": {
                                                "sFirst": "Primeiro",
                                                "sPrevious": "Anterior",
                                                "sNext": "Próximo",
                                                "sLast": "Último"
                                            }
                                        },
                                        processing: true,
                                        serverSide: true,
                                        responsive: true,
                                        "ajax": {
                                            "url": "{!! route('funcionario.data') !!}",
                                            "type": "POST"
                                        },
                                        columns: [
                                            {data: 'ID_PESSOA', name: 'ID_PESSOA'},
                                            {data: 'NM_PESSOA', name: 'NM_PESSOA'},
                                            {data: 'contato.DS_EMAIL', name: 'contato.DS_EMAIL'},
                                            {data: 'contato.DS_TEL_01', name: 'contato.DS_TEL_01'},
                                            {data: 'contato.DS_CEL_01', name: 'contato.DS_CEL_01'},
                                            {data: 'acoes', name: 'acoes', orderable: false, searchable: false}
                                        ]
                                    });
                                });
                            </script>
                            <script type="text/javascript">
                                //$('#uf').ufs();
                                $("#uf").focus(function () {
                                    $('#uf').ufs();
                                });
                                $(document).ready(function () {
                                    $('.data').mask('00/00/0000');
                                    $('.cpf').mask('000.000.000-00', {reverse: true});
                                    $('.cep').mask('00000-000');
                                    $('.cel').mask('(00) 00000-0000');
                                    $('.tel').mask('(00) 0000-0000');
                                });
                            </script>
                            {{--@endpush--}}
                        </div>
                    </div><!-- /.box-body -->
                </form>
            </div><!-- /.box -->
        </div>
    </div>
@endsection


