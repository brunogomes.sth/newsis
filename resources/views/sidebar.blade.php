
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class="header">Menu</li>
            <li class=""><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> <span>Início</span></a></li>
            <li class=""><a href="{{route('atendimento')}}"><i class="fa fa-link"></i> <span>Atendimentos</span></a></li>

            <li class=""><a href=""><i class="fa fa-unlock"></i> <span>Senhas</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Cadastros</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('clientes')}}">Clientes</a></li>
                    <li><a href="{{route('programas')}}">Programas</a></li>
                    <li><a href="#">Vendedores/Representantes</a></li>
                </ul>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->

</aside>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class="header">Menu</li>
            <li class=""><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> <span>Início</span></a></li>
            <li class=""><a href="{{route('atendimento')}}"><i class="fa fa-link"></i> <span>Atendimentos</span></a></li>
            <li class=""><a href="{{route('agenda')}}"><i class="fa fa-phone"></i> <span>Agenda</span></a></li>
            <li class=""><a href=""><i class="fa fa-unlock"></i> <span>Senhas</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Cadastros</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('clientes')}}">Clientes</a></li>
                    <li><a href="{{route('programas')}}">Programas</a></li>
                    <li><a href="{{route('funcionario')}}">Funcionários</a></li>
                    <li><a href="{{route('usuario')}}">Usuários</a></li>
                    <li class="treeview">
                        <a href="#"> <span>Outros</span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('gc_create')}}">Grupos de Cliente </a></li>
                            <li><a href="{{route('se_create')}}">Setores da Empresa</a></li>
                            <li><a href="{{route('st_create')}}">Status</a></li>
                            <li><a href="{{route('ca_create')}}">Cargos</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="header"></li>
            <li class="">
                <a href="{{route('logout')}}"><i class="fa fa-power-off"></i> <span>Sair</span></a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->

</aside>
