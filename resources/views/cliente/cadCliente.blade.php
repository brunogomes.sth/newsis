@extends('admin_template')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ isset($pessoas->ID_PESSOA) ? 'Editar Cliente' : 'Cadastrar Cliente' }}</h3>
            </div>
            <div class="box-body ">
                <form action="{{ isset($pessoas->ID_PESSOA) ? route('updateCliente', $pessoas->ID_PESSOA) : route('salvarCliente') }}"
                  method="POST">
                  <div class="row">
                    <div class="col-md-12">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                            </button>
                            <ul class="list-unstyled">
                                @foreach ($errors->all() as $error)
                                <li><i class="fa fa-caret-right "> </i> {{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" role="tablist">
                        <li id="" role="presentation" class="active"><a href="#dados" aria-controls="dados"
                            role="tab" data-toggle="tab">Dados</a>
                        </li>
                        <li id="" role="presentation"><a href="#contatos" aria-controls="telefones" role="tab"
                           data-toggle="tab">Contatos</a></li>
                           <li role="presentation"><a onclick="" href="#endereco" aria-controls="endereco"
                             role="tab" data-toggle="tab">Endereço</a></li>
                             <li role="presentation"><a href="#financeiro" aria-controls="financeiro" role="tab"
                                 data-toggle="tab">Financeiro</a></li>
                                 <li role="presentation"><a href="#outras" aria-controls="outras" role="tab"
                                     data-toggle="tab">Outras Informações</a></li>
                                 </ul>
                                 <!-- Tab panes -->
                                 <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="dados">
                                        {{Form::hidden('FL_CLIENTE', '1')}}
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Codigo:</label>
                                                    <input type="text" class="form-control input-sm "
                                                    value="{{ isset($pessoas->ID_PESSOA) ? $pessoas->ID_PESSOA: '' }}"
                                                    disabled="disabled">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Tipo Pessoa:</label>
                                                    <select class="form-control input-sm" id="tp_pessoa" name="TP_PESSOA"
                                                    onclick="pessoa(this.value)">
                                                    <option id="Fisica"
                                                    <?= isset($pessoas->TP_PESSOA) && $pessoas->TP_PESSOA == '1' || old('TP_PESSOA') == '1' ? "selected=''" : "" ?> value="1">
                                                    1 - Pessoa Física
                                                </option>
                                                <option id="Juridica"
                                                <?= isset($pessoas->TP_PESSOA) && $pessoas->TP_PESSOA == '2' || old('TP_PESSOA') == '2' ? "selected=''" : "" ?>  value="2">
                                                2 - Pessoa Jurídica
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Ativo</label><br>
                                        <label class="radio-inline">
                                            {{ Form::radio('FL_ATIVO', '1', isset($pessoas->TP_PESSOA) && $pessoas->FL_ATIVO == '1' || (old('FL_ATIVO') == '1'), array('id'=>'FL_ATIVO_SIM'))}}
                                            Sim
                                        </label>
                                        <label class="radio-inline">
                                            {{ Form::radio('FL_ATIVO', '0',isset($pessoas->TP_PESSOA) && $pessoas->FL_ATIVO == '0' ||  (old('FL_ATIVO') == '0'), array('id'=>'FL_ATIVO_NAO'))}}
                                            Não
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Grupo de Cliente:</label>
                                        <select class="form-control input-sm" name="ID_GRUPO_CLIENTE">
                                            <option <?= old('ID_GRUPO_CLIENTE') == '' ? "selected=''" : "" ?> value="">
                                                Selecione...
                                            </option>
                                            @foreach( App\GrupoCliente::get() as $grupo_cli)
                                            <option <?php echo ((old('ID_GRUPO_CLIENTE') == $grupo_cli->ID_GRUPO_CLIENTE) || (isset($pessoas->ID_GRUPO_CLIENTE) && $pessoas->ID_GRUPO_CLIENTE == $grupo_cli->ID_GRUPO_CLIENTE)) ? "selected=''" : ""; ?> value="{{$grupo_cli->ID_GRUPO_CLIENTE}}">
                                                {{$grupo_cli->NM_GRUPO_CLIENTE}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="lbNomeRazao">Nome:</label>
                                        <input type="text" class="form-control input-sm" name="NM_PESSOA"
                                        value="{{$pessoas->NM_PESSOA or old('NM_PESSOA') }}">
                                    </div>
                                </div>
                                <div id="pf" style="display: block;">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>RG:</label>
                                            <input type="text" class="form-control input-sm" name="CD_RG"
                                            value="{{$pessoas->fisica->CD_RG or old('CD_RG') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>CPF:</label>
                                            <input type="text" class="form-control input-sm mask cpf"
                                            name="CD_CPF"
                                            value="{{$pessoas->fisica->CD_CPF or old('CD_CPF') }}">
                                        </div>
                                    </div>
                                </div>
                                <div id="pj" style="display: none;">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nome Fantasia:</label>
                                            <input type="text" class="form-control input-sm"
                                            name="NM_FANTASIA"
                                            value="{{$pessoas->juridica->NM_FANTASIA or old('NM_FANTASIA') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>CNPJ:</label>
                                            <input type="text" class="form-control input-sm mask cnpj"
                                            name="CD_CNPJ"
                                            value="{{$pessoas->juridica->CD_CNPJ or old('CD_CNPJ') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>CGF:</label>
                                            <input type="text" class="form-control input-sm" name="CD_CGF"
                                            value="{{$pessoas->juridica->CD_CGF or old('CD_CGF')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Responsavel:</label>
                                            <input type="text" class="form-control input-sm"
                                            name="NM_RESPONSAVEL"
                                            value="{{$pessoas->juridica->NM_RESPONSAVEL or old('NM_RESPONSAVEL') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="contatos">
                            {{Form::hidden('TP_PESSOA_CONTATO', 'C')}}
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Telefone 1:</label>
                                        <input type="text" class="form-control input-sm mask tel"
                                        name="DS_TEL_01"
                                        value="{{$pessoas->contato->DS_TEL_01 or old('DS_TEL_01') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Telefone 2:</label>
                                        <input type="text" class="form-control input-sm mask tel"
                                        name="DS_TEL_02"
                                        value="{{$pessoas->contato->DS_TEL_02 or old('DS_TEL_02') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Celular 1:</label>
                                        <input type="text" class="form-control input-sm mask cel"
                                        name="DS_CEL_01"
                                        value="{{$pessoas->contato->DS_CEL_01 or old('DS_CEL_01') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Celular 2:</label>
                                        <input type="text" class="form-control input-sm mask cel"
                                        name="DS_CEL_02"
                                        value="{{$pessoas->contato->DS_CEL_02 or old('DS_CEL_02') }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>E-mail:</label>
                                        <input type="text" class="form-control input-sm" name="DS_EMAIL"
                                        value="{{$pessoas->contato->DS_EMAIL or old('DS_EMAIL') }}">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="endereco">
                            <div class="row">

<!-- JAVASCRIP PREENCHER CIDADE/ESTADO | CASO CAIA EM VALIDATIONS TBM -->
<input type="hidden" id="state" data-field-id="{{ $pessoas->logradouro->bairro->cidade->ID_ESTADO or old('UF') }}" >
<input type="hidden" id="city" data-field-id="{{ $pessoas->logradouro->bairro->cidade->ID_CIDADE or old('ID_CIDADE') }}" >   
<input type="hidden" id="urlcidades" data-field-url="{{ url('/cidades/') }}" >
<input type="hidden" id="urlestados" data-field-url="{{ url('/ufs/') }}" >
<script src="{{ asset ("/js/getEstados.js") }}"></script>
<script src="{{ asset ("/js/jquery-ui.js") }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset ("/css/jquery-ui.css") }}"> 

<script type="text/javascript">

   var state = $("#state").data('field-id');
   var city  = $("#city").data('field-id');

   if(state > 0)
       getEstados(state, city);
   else
       ufs();
</script>

<!--FIM CHAMADAS JAVASCRIPTS-->
<div class="col-md-2">
    <div class="form-group">
        <label>UF:</label>
        <select id="uf" class="form-control input-sm" name="UF" default="CE">
            @if(isset($cidades))
            @foreach($cidades as $cidade)
            <option value="{{$cidade->ID_ESTADO}}">{{$cidade->SG_ESTADO}}</option>
            @endforeach
            @endif
        </select>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label>Cidade:</label>
        <select class="form-control input-sm" name="ID_CIDADE" id="cidade">
            @if(isset($cidades))
            @foreach($cidades as $cidade)
            <option value="{{$cidade->ID_CIDADE}}">{{$cidade->NM_CIDADE}}</option>
            @endforeach
            @endif
        </select>
    </div>
</div>
    <div class="col-md-6">
        <div class="form-group">
            <label >Endereço:</label>
            <input type="text"  id="logradouro" data-field-url="{{ url('logradouro/cidade/{id}/logradouro/') }}" class="form-control input-sm" name="NM_LOGRADOURO"
            value="{{$pessoas->logradouro->NM_LOGRADOURO or old('NM_LOGRADOURO') }}">

            <input type="hidden" name="auto" id="auto" />
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-4">
        <div class="form-group">
            <label>Bairro:</label>
            <input type="text" id="bairro" class="form-control input-sm" name="NM_BAIRRO"
            value="{{$pessoas->logradouro->bairro->NM_BAIRRO or old('NM_BAIRRO') }}">
        </div>
    </div>

    <script src="{{ asset ("/js/logradouro.js") }}"></script>

    <div class="col-md-2">
        <div class="form-group">
            <label for="cep">CEP:</label>
            <input type="text" class="form-control input-sm mask cep" name="DS_CEP"
            id="cep"
            value="{{$pessoas->logradouro->DS_CEP or old('DS_CEP') }}">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <label>Nº:</label>
            <input type="text" class="form-control input-sm" name="NN_NUMERO"
            value="{{$pessoas->NN_NUMERO or old('NN_NUMERO') }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Complemento:</label>
            <input type="text" class="form-control input-sm" name="DS_COMPLEMENTO"
            value="{{$pessoas->DS_COMPLEMENTO or old('DS_COMPLEMENTO') }}">
        </div>
    </div>
</div>
</div>
<div role="tabpanel" class="tab-pane" id="financeiro">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label>Liberar Senha:</label><br>
                <label class="radio-inline">
                    <input type="radio"
                    <?= isset($pessoas->FL_DAR_SENHA) && $pessoas->FL_DAR_SENHA == '1' || old('FL_DAR_SENHA') == '1' ? "checked=''" : "" ?> name="FL_DAR_SENHA"
                    id="lib_senha_sim" value="1"> Sim
                </label>
                <label class="radio-inline">
                    <input type="radio"
                    <?= isset($pessoas->FL_DAR_SENHA) && $pessoas->FL_DAR_SENHA == '0' || old('FL_DAR_SENHA') == '0' ? "checked=''" : "" ?> name="FL_DAR_SENHA"
                    id="lib_senha_nao" value="0"> Não
                </label>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group ">
                <label>Contrato:</label><br>
                <label class="radio-inline">
                    <input type="radio"
                    <?= isset($pessoas->FL_CONTRATO) && $pessoas->FL_CONTRATO == '1' || old('FL_CONTRATO') == '1' ? "checked=''" : "" ?>  name="FL_CONTRATO"
                    id="tp_atendimento_contrato" value="1"> Sim
                </label>
                <label class="radio-inline">
                    <input type="radio"
                    <?= isset($pessoas->FL_CONTRATO) && $pessoas->FL_CONTRATO == '0' || old('FL_CONTRATO') == '0' ? "checked=''" : "" ?> name="FL_CONTRATO"
                    id="tp_atendimento_visita" value="0"> Não
                </label>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group ">
                <label>Prazo de Senhas:</label><br>
                <label class="radio-inline">
                    {{ Form::radio('PRAZO', '52', isset($pessoas->PRAZO) && $pessoas->PRAZO == '52' || (old('PRAZO') == '52'), array('id'=>'PRAZO_ANUAL'))}}
                    Anual
                </label>
                <label class="radio-inline">
                    {{ Form::radio('PRAZO', '02',isset($pessoas->PRAZO) && $pessoas->PRAZO == '02' ||  (old('PRAZO') == '02'), array('id'=>'PRAZO_DEMO'))}}
                    Demo
                </label>
                <label class="radio-inline">
                    {{ Form::radio('PRAZO', '09',isset($pessoas->PRAZO) && $pessoas->PRAZO == '09' ||  (old('PRAZO') == '09'), array('id'=>'PRAZO_SEMESTRAL'))}}
                    Semestral
                </label>
                <label class="radio-inline">
                    {{ Form::radio('PRAZO', '26',isset($pessoas->PRAZO) && $pessoas->PRAZO == '26' ||  (old('PRAZO') == '26'), array('id'=>'PRAZO_TRIMESTRAL'))}}
                    Trimestral
                </label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group ">
                <label>Suporte Grátis:</label><br>
                <div class="input-group ">
                    <span class="input-group-addon input-sm">
                        <input type="checkbox">
                    </span>
                    {{ Form::date('name', \Carbon\Carbon::now(),  array('class' => 'form-control input-sm'))}}
                </div>
                <!-- /input-group -->
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Vendedor/Representante:</label>
                <?php $funcionarios = DB::table('PESSOA')->select('ID_PESSOA', 'NM_PESSOA')->where('FL_FUNCIONARIO', 1)->where('FL_ATIVO', 1)->get(); ?>
                <select name="CD_VEND_REPR" class="form-control input-sm">
                    <option value="0">Selecione...</option>
                    <?php foreach ($funcionarios as $funcionario): ?>
                        <option value="<?php echo $funcionario->ID_PESSOA ?>" <?php
                            if (isset($pessoas) and $pessoas->CD_VEND_REPR == $funcionario->ID_PESSOA) {
                                echo 'selected';
                            }
                            ?> >{{$funcionario->NM_PESSOA}}</option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="outras">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Obs. (Opcional):</label>
                    <textarea class="form-control input-sm" name="DS_OBS"
                    rows="6">{{$pessoas->DS_OBS or old('DS_OBS') }}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Bases de Dados:</label>
                    <textarea class="form-control input-sm" name="DS_BASE"
                    rows="6">{{$pessoas->DS_BASE or old('DS_BASE') }}</textarea>
                    <small class="help-block">Ex.: PDV: C:\sistech\dados\dados.fdb</small>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="box-footer clearfix">
    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
    <a class="btn btn-danger" href="{{route('clientes')}}"><i class="fa fa-sign-in"></i> Voltar</a>
</div>
</div>
</div>
</form>
<!-- Fim Modal -->
</div><!-- /.box -->
<script>

         $("#uf").change(function () {
        cidade($("#uf").val(), null , $("#urlcidades").data('field-url') );

    });

        //verifica se é pessoa fisica ou juridica, mostrando assim seus respectivos formularios.
        var e = document.getElementById("tp_pessoa");
        //var itemSelecionado = e.options[e.selectedIndex].value;
        if (e.options[e.selectedIndex].value == "1") {

            document.getElementById("lbNomeRazao").innerHTML = "Nome:";
            document.getElementById("pf").style.display = "block";
            document.getElementById("pj").style.display = "none";
        }
        else if (e.options[e.selectedIndex].value == "2") {
            document.getElementById("lbNomeRazao").innerHTML = "Razão Social:";
            document.getElementById("pf").style.display = "none";
            document.getElementById("pj").style.display = "block";
        }
        function pessoa(tipo) {
            if (tipo == "1") {
                document.getElementById("lbNomeRazao").innerHTML = "Nome:";
                document.getElementById("pf").style.display = "block";
                document.getElementById("pj").style.display = "none";
            } else if (tipo == "2") {
                document.getElementById("lbNomeRazao").innerHTML = "Razão Social:";
                document.getElementById("pf").style.display = "none";
                document.getElementById("pj").style.display = "block";
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.data').mask('00/00/0000');
        });
        $(".mask").focus(function () {
            $('.cpf').mask('000.000.000-00', {reverse: true});
            $('.cep').mask('00000-000');
            $('.cel').mask('(00) 00000-0000');
            $('.tel').mask('(00) 0000-0000');
            $('.cnpj').mask('00.000.000/0000-00');
        });
        $(".mask").blur(function () {
            $('.cpf').unmask();
            $('.cep').unmask();
            $('.cel').unmask();
            $('.tel').unmask();
            $('.cnpj').unmask();
        });
    </script>
    @endsection
