@extends('admin_template')
@section('content')

<!-- Main content -->
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-desktop"></i> Detalhes do Cliente.
                <small class="pull-right">Date: {{date('d/m/Y')}}</small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <!-- Dados do identificação funcionario-->
    <div class="row invoice-info">
        <div class="col-xs-12">
            <legend>Cliente</legend>
            <div class="col-sm-1 invoice-col">
                <strong>Código:</strong>
                <div class="lead">
                    {{$pessoas->ID_PESSOA}}

                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6 invoice-col">
                <strong>Cliente:</strong>
                <div class="lead">
                    {{$pessoas->NM_PESSOA}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-2 invoice-col">
                <strong>Grupo:</strong>
                <div class="lead">
                   <?php $gru_cli = App\GrupoCliente::where('ID_GRUPO_CLIENTE', $pessoas->ID_GRUPO_CLIENTE)->first()?>
                   {{$gru_cli->NM_GRUPO_CLIENTE}}
                </div>
            </div>
            <div class="col-sm-3 invoice-col">
                <strong>Cadastro:</strong>
                <div class="lead ">
                    {{ date('d/m/Y H:m:s', strtotime($pessoas->created_at)) }}  
                </div>
            </div>
        </div>
    </div>
    @if ($pessoas->TP_PESSOA == 1)
       <div class="row invoice-info">
        <div class="col-xs-12">

            <div class="col-sm-2 invoice-col">
                <strong>CPF:</strong>
                <div class="lead">
                    {{mask($pessoas->fisica->CD_CPF, "###.###.###-##")}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-5 invoice-col">
                <strong>RG:</strong>
                <div class="lead">
                    {{$pessoas->fisica->CD_RG}}
                </div>
            </div>
           
        </div>
    </div>           
    @else
        <div class="row invoice-info">
        <div class="col-xs-12">

            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>Nome Fantasia:</strong>
                <div class="lead">
                    {{$pessoas->juridica->NM_FANTASIA}}
                </div>
            </div>
            <div class="col-sm-2 invoice-col">
                <strong>CNPJ:</strong>
                <div class="lead">

                    {{mask($pessoas->juridica->CD_CNPJ, "##.###.###/####-##")}}

                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-2 invoice-col">
                <strong>CGF:</strong>
                <div class="lead">
                    {{$pessoas->juridica->CD_CGF}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-2 invoice-col">
                <strong>Responsavel:</strong>
                <div class="lead">
                    {{$pessoas->juridica->NM_RESPONSAVEL}}
                </div>
            </div>
           
        </div>
    </div>
    @endif
    <!-- /.row -->
    <!-- Dados Contato do funcionario-->
    <div class="row invoice-info">
        <div class="col-xs-12">
            <legend>Contatos</legend>
            <div class="col-sm-4 invoice-col">
                <strong class="text-center">Telefones:</strong>
                <div class="lead">
                    @if( !empty($pessoas->contato->DS_TEL_01) )
                        {{mask($pessoas->contato->DS_TEL_01, "(##) ####-####")}}
                    @else
                        {{'-'}}
                    @endif

                    @if(  !empty( $pessoas->contato->DS_TEL_02) )
                        / {{mask($pessoas->contato->DS_TEL_02, "(##) ####-####")}}
                    @else
                        {{'/ -'}}
                    @endif
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>Celulares:</strong>
                <div class="lead">

                @if( !empty( $pessoas->contato->DS_CEL_02 ) )
                    {{mask($pessoas->contato->DS_CEL_02, "(##) #####-####")}} 
                @endif    

                @if( !empty($pessoas->contato->DS_CEL_01) )
                    /{{mask($pessoas->contato->DS_CEL_01, "(##) #####-####")}} 
                @endif
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <strong>E-mail:</strong>
                <div class="lead">
                    {{$pessoas->contato->DS_EMAIL}}
                </div>
            </div>
            <!-- /.col -->
        </div>
    </div>
    <!-- /.row -->
    <!-- Dados Endereco do funcionario-->
    <div class="row invoice-info">
        <div class="col-xs-12">
            <legend>Endereço</legend>
            <div class="col-sm-3 invoice-col">
                <strong class="text-center">CEP:</strong>
                <div class="lead">
                    {{$pessoas->logradouro->DS_CEP}}
                </div>
            </div>
            <!-- /.col -->

            <div class="col-sm-3 invoice-col">
                <strong>UF:</strong>
                <div class="lead">
                    {{$pessoas->logradouro->bairro->cidade->estado->SG_ESTADO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>Cidade:</strong>
                <div class="lead">
                    {{$pessoas->logradouro->bairro->cidade->NM_CIDADE}}
                </div>
            </div>
            <div class="col-sm-3 invoice-col">
                <strong>Bairro:</strong>
                <div class="lead">
                    {{$pessoas->logradouro->bairro->NM_BAIRRO}}
                </div>
            </div>
        </div>
    </div>
    <!-- /.col -->
    <!-- Dados Enreco do funcionario-->
    <div class="row invoice-info">
        <div class="col-xs-12">
            <div class="col-sm-4 invoice-col">
                <strong class="text-center">Logradouro:</strong>
                <div class="lead">
                    {{$pessoas->logradouro->NM_LOGRADOURO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>Número:</strong>
                <div class="lead">
                    {{$pessoas->NN_NUMERO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>Complemento:</strong>
                <div class="lead">
                    {{$pessoas->DS_COMPLEMENTO}}                
                </div>
            </div>
        </div>
    </div>
    <div class="row invoice-info">
        <div class="col-xs-12">
            <legend>Financeiro</legend>
            <div class="col-sm-1 invoice-col">
                <strong>Liberar Senha:</strong>
                <div class="lead">
                @if ($pessoas->FL_DAR_SENHA > 0)
                        {{'Sim'}}
                 @else
                    {{'Não'}}
                @endif
                </div>
            </div>

            <div class="col-sm-1 invoice-col">
                <strong>Status:</strong>
                <div class="lead">
                    @if ($pessoas->FL_ATIVO > 0)
                    {{'Ativo'}}
                    @else
                    {{'Inativo'}}
                    @endif    
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-3 invoice-col">
                <strong>Contato:</strong>
                <div class="lead">
                    {{$pessoas->contato->NM_CONTATO}}
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-2 invoice-col">
                <strong>Prazo:</strong>
                <div class="lead">
                 {{$prazos[ (int) $pessoas->PRAZO]}}
                </div>
            </div>
            <div class="col-sm-2 invoice-col">
                <strong>Suporte Grátis:</strong>
                <div class="lead">
                    {{$pessoas->DT_CADASTRO}}
                </div>
            </div>
            <div class="col-sm-3 invoice-col">
                <strong>Vendedor/Representante:</strong>
                <div class="lead">
                    <?php
                    $funcionarios = DB::table('PESSOA') ->select('ID_PESSOA', 'NM_PESSOA')->where('FL_FUNCIONARIO', 1)->where('FL_ATIVO', 1)->where('ID_PESSOA', $pessoas->CD_VEND_REPR)->first();
                    ?>
                        {{$funcionarios->NM_PESSOA or ''}}
                </div>
            </div>
        </div>
    </div>
    <!--PROGRAMAS-->
    <!-- /.row -->
    <!-- this row will not appear when printing -->
    <div class="row no-print">
         <div class="col-xs-12">
         <legend>Programas</legend>
              @include('programa_cliente.mostraProgramas')
        </div>
    </div>
    <div class="row no-print">
       <div class="col-xs-12">
            <hr>
            <a class="btn btn-danger  pull-right" href="{{route('clientes')}}"><i class="fa fa-sign-in"></i> Voltar</a>
            <a class="btn bg-orange pull-right" style="margin-right: 5px;" href="{{route('programaCliente', $pessoas->ID_PESSOA)}}"><i class="fa fa-desktop"></i> Ver Programas</a>
            <a class="btn btn-primary pull-right editar" style="margin-right: 5px;"
               href="{{route('editCliente', $pessoas->ID_PESSOA)}}">
                <i class="fa fa-edit"></i> Editar
            </a>
        </div>
    </div>
</section>
@endsection