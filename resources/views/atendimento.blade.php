@extends('admin_template')

@section('content')

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-app">
            <i class="fa fa-plus"></i> Novo
        </a>
        <a class="btn btn-app">
            <i class="fa fa-edit"></i> Alterar
        </a>
      
        <a class="btn btn-app">
            <i class="fa fa-child"></i> Finalizar
        </a>
        <a class="btn btn-app">
            <i class="fa fa-unlock"></i> Senhas
        </a>
        <a class="btn btn-app">
            
            <i class="fa fa-user"></i> Consultar Cliente
        </a>
        
    </div>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
           

            <div class="box-body with-border ">

            </div><!-- /.box-body -->

            <div class="box-footer clearfix">

            </div>

        </div><!-- /.box -->

    </div>
</div>


@endsection