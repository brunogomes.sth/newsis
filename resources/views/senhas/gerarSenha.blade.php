@extends('admin_template')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <form method="post" action="{{route('programaClienteAdd')}}" id="formulario">
                {!! csrf_field() !!}
                <div class="box-header with-border">
                    <h3 class="box-title">Programas utilizados pelo cliente: # {{$pessoas->ID_PESSOA}}</h3>
                </div>
                <div class="box-body ">
                    <div class="row">
                         {{Form::hidden('ID_PESSOA', $pessoas->ID_PESSOA)}}
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Programa:</label> 
                                        <select id="programas" name=""  class="form-control  input-sm" required="">
                                            <option value="">Selecione um Programa</option>
                                        </select>                                                                
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Versão:</label> 

                                        <select id="versao" name="ID_VERSION" class="form-control input-sm" required="" ></select>
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="color: #fff">&nbsp; &nbsp;</label>
                                        <br>
                                        <button type="submit"  class="btn btn-primary btn-sm">
                                            <span class="glyphicon glyphicon-plus"  aria-hidden="true"></span> <strong>Adcionar</strong>
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 table-responsive ">
                            <table id="products-table" class="table table-bordered table-striped "> 
                                <thead> 
                                    <tr class= "bg-light-blue"> 
                                        <th>#</th> 
                                        <th>Programa</th> 
                                        <th>Versão</th> 
                                        <th>Plataforma</th> 
                                        <th class="col-md-1 text-center">Excluir</th> 
                                    </tr> 
                                </thead> 
                                <tbody> 
                                    @foreach ($cliPrg as $cliPr)
                                    <tr>
                                        <td>{{$cliPr->ID_PROGRAMA_PESSOA}}</td>
                                        <td>{{$cliPr->NM_PROGRAMA}}</td>
                                        <td>{{$cliPr->DS_VERSION}}</td>
                                        <td>{{$cliPr->DS_PLATAFORM}}</td>
                                        <td class="text-center"><a type="button" href="{{route('programaClienteRemove', $cliPr->ID_PROGRAMA_PESSOA)}}"  class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></a></td>
                                    </tr>
                                    @endforeach
                                </tbody> 
                            </table>
                        </div>

                    </div>
                </div>
               
            </form>
        </div>        
    </div>
</div>
<script src="{{ asset ("js/programa.js") }}"></script>

<script>

$.fn.programa = function (options) {

    var select = $(this);
    var settings = $.extend({
        'default': select.attr('default')
    }, options);
    $.get("/getPrograma", null, function (json) {

        $.each(json, function (key, value) {
            select.append('<option value="' + value.ID_PROGRAMA + '" id="' + value.DS_PLATAFORM + '" ' + (settings.default == value.ID_PROGRAMA ? 'selected' : '') + ' label ="' + value.NM_PROGRAMA + '" ></option>');
        })


        versao(select.val());
    }, 'json');
    select.change(function () {
        versao(select.val());
    });
};
function versao(options) {


    var select = $("#versao");
    var settings = $.extend({
        'default': select.attr('default'),
        'ID_PROGRAMA': (options == 'undefined') ? null : options
    }, options);
    if (settings.ID_PROGRAMA == null)
        console.warn('Nenhum Programa informado');
    else {

        select.html('<option value="">Carregando..</option>');
        $.get("/getVersion/" + settings.ID_PROGRAMA, null, function (json) {

            select.html('<option value="">Selecione</option>');
            $.each(json, function (key, value) {
                select.append('<option value="' + value.ID_VERSION + '" ' + ((settings.default == value.ID_VERSION || settings.default == value.ID_VERSION) ? 'selected' : '') + '>' + value.DS_VERSION + '</option>');
            })

        }, 'json');
    }
}
;
</script>


<script type="text/javascript">

    $('#programas').programa();

</script>
@endsection
