@extends('admin_template')
@section('content')
<div class="row">
    <div class="col-md-6">
        <!-- BOX GRUPO DE CLIENTE-->
        @include('outros.grupoCliente')

        <!-- BOX SETORES-->
        @include('outros.setores')
    </div><!-- fim da primeira coluna-->

    <!-- Segunda Coluna-->
    <div class="col-md-6">
        <!-- BOX STATUS-->
        @include('outros.status')

        <!-- BOX CARGOS-->
        @include('outros.cargos')
    </div>
</div>
<script type="text/javascript">

        $('.salvar').click(function(event){

            event.preventDefault();

            var dados = { [ $(this).get(0).form[1].name ] : $(this).get(0).form[1].value };

            var form = ( $(this).get(0).form.id);

            if($(this).get(0).form[0].value != '') {
                dados  = { [$(this).get(0).form[1].name ] : $(this).get(0).form[1].value,
                           [$(this).get(0).form[0].name ] : $(this).get(0).form[0].value };
            }

            var action = "'" + $(this).get(0).form.action  + "'";
            

            $.ajax({
                type: $(this).get(0).form.method,
                url : $(this).get(0).form.action,
                data: dados ,
                success: function(s)
                {   
                    if(action.search("update") > 0){
                        location.href = "{{url('outros')}}";
                    }
                    if(form == "grupo") 
                        grupo.ajax.reload();
                    else if(form == "setores")
                        setores.ajax.reload();
                    else if(form == "CadStatus")
                        CadStatus.ajax.reload();
                    else
                        cargos.ajax.reload();
                },
                error: function(e)
                {
                    console.log(e);
                }  
            });

        });





</script>
@endsection


