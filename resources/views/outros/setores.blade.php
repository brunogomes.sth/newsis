@extends('admin_template')
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Setores da Empresa</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <form  method="post" id="setores"
                   action="{{isset($setor->ID_SETOR) ? route('se_atualizar',$setor->ID_SETOR) : route('se_salvar')}}">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="status">Nome do Setor:</label>
                        <input type="text" class="form-control input-sm text-uppercase" name="NM_SETOR" required  id="status"
                               value="{{$setor->NM_SETOR or ''}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>&nbsp;</label><br>
                        <input type="submit" class="btn btn-sm btn-success salvar" value="Salvar">
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <form role="form">
                <div class="box-body ">
                    <div class="table-responsive">
                        <table id="setores_table" class="table table-bordered table-hover">
                            <thead>
                            <tr class="bg-light-blue">
                                <th style="width: 1px;">#</th>
                                <th style="">Nome</th>
                                <th class="text-center" rowspan="1" colspan="1" aria-label="Action" style="width: 5px;">Ações</th>
                            </tr>
                            </thead>
                        </table>
                        {{--@push('scripts')--}}
                        <script>
                            var setores = $('#setores_table').DataTable({
                                    "paging": true,
                                    "lengthChange": false,
                                    "searching": false,
                                    "ordering": true,
                                    "info": false,
                                    "autoWidth": true,
                                    "oLanguage": {
                                        "sProcessing": "Aguarde enquanto os dados são carregados ...",
                                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                                        "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                                        "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                                        "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                                        "sInfoFiltered": "",
                                        "sSearch": "Procurar",
                                        "oPaginate": {
                                            "sFirst": "<<",
                                            "sPrevious": "<",
                                            "sNext": ">",
                                            "sLast": ">>"
                                        }
                                    },
                                    processing: true,
                                    serverSide: true,
                                    "ajax": {
                                        "url": "{!! route('setores.data') !!}",
                                        "type": "POST"
                                    },
                                    columns: [
                                        {data: 'ID_SETOR', name: 'ID_SETOR', class:'text-right'},
                                        {data: 'NM_SETOR', name: 'NM_SETOR'},
                                        {data: 'acoes', name: 'acoes', orderable: false, searchable: false}
                                    ]
                                });
                            
                        </script>
                        {{--@endpush--}}
                    </div>
                </div><!-- /.box-body -->
            </form>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
@endsection