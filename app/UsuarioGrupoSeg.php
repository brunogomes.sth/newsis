<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioGrupoSeg extends Model
{
    //
    protected $table = 'USUARIO_GRUPO_SEG';
    public $timestamps = true;
    protected $primaryKey = 'ID_USUARIO_GRUPO';
    protected $fillable = ['ID_USUARIO','ID_GRUPO_SEG'];
}
