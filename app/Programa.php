<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programa extends Model {

    protected $table = 'PROGRAMA';
    public $timestamps = true;
    protected $primaryKey = 'ID_PROGRAMA';
    protected $fillable = ['NM_PROGRAMA','DT_CADASTRO','DS_PLATAFORM'];
    
    //relacionamento com versão
    public function version(){
        return $this->hasMany('App\Version', 'ID_PROGRAMA', 'ID_PROGRAMA');
    }
}
