<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoCliente extends Model
{
    //
    protected $table = 'GRUPO_CLIENTE';
    public $timestamps = false;
    protected $primaryKey = 'ID_GRUPO_CLIENTE';
    protected $fillable = ['NM_GRUPO_CLIENTE'];
}
