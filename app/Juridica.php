<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Juridica extends Model
{
   
    protected  $table = 'JURIDICA';
    public 	   $timestamps = true;
    protected  $primaryKey = 'ID_JURIDICA';
    protected  $fillable = ['ID_PESSOA','NM_FANTASIA','CD_CNPJ','CD_CGF','NM_RESPONSAVEL'];


    public function pessoa()
    {
    	return $this->belongsTo('App\Pessoa', 'ID_PESSOA');
    }

    public function getFillables()
    {
    	return array_flip($this->fillable);
    }
    
}

