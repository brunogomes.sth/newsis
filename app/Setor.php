<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setor extends Model
{
    //
    protected $table = 'SETOR';
    public $timestamps = true;
    protected $primaryKey = 'ID_SETOR';
    protected $fillable = ['NM_SETOR'];
}
