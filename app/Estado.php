<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model {

    //
    protected $table = 'ESTADO';
    public $timestamps = true;
    protected $primaryKey = 'ID_ESTADO';
    protected $fillable = ['NM_ESTADO', 'SG_ESTADO', 'DS_PAIS'];

    public function cidades() {
        return $this->hasMany('App\Cidade');
    }


}
