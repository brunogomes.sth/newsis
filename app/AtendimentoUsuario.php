<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtendimentoUsuario extends Model
{
    //
    protected $table = 'ATENDIMENTO_USUARIO';
    public $timestamps = true;
    protected $primaryKey = 'ID_ATEND_USUARIO';
    protected $fillable = ['ID_ATENDIMENTO','ID_USUARIO'];
}
