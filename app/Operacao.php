<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operacao extends Model
{
    //
    protected $table = 'OPERACAO';
    public $timestamps = true;
    protected $primaryKey = 'ID_OPERACAO';
    protected $fillable = ['CD_OPERACAO','NM_OPERACAO','DS_OPERACAO'];
}
