<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    //
    protected $table = 'CARGO';
    public $timestamps = true;
    protected $primaryKey = 'ID_CARGO';
    protected $fillable = ['NM_CARGO'];
}
