<?php

namespace App;


use DB;
use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    //
    protected $table = 'PESSOA';
    protected $primaryKey = 'ID_PESSOA';
    protected $fillable = ['TP_PESSOA','NM_PESSOA',
                           'NN_NUMERO','DS_COMPLEMENTO','DT_CADASTRO','DS_OBS',
                           'ID_GRUPO_CLIENTE','DS_BASE','FL_ATIVO',
                           'FL_DAR_SENHA','FL_CLIENTE','FL_FUNCIONARIO',
                           'DT_FINAL_GRATIS','DT_INICIAL_GRATIS','FL_CONTRATO',
                            'PRAZO', 'ID_LOGRADOURO'
                            ];
    
    public function funcionario(){
        return $this->hasOne('App\Funcionario','ID_PESSOA');
    }

    public function contato(){
        return $this->hasOne('App\Contatos','ID_PESSOA');
    }

    public function juridica()
    {
        return $this->hasOne('App\Juridica', 'ID_PESSOA');
    }

    public function fisica()
    {
        return $this->hasOne('App\Fisica', 'ID_PESSOA');
    }
    public function logradouro()
    {
        return $this->belongsTo('App\Logradouro', 'ID_LOGRADOURO');
    }

    public function getFillables()
    {
        return array_flip($this->fillable);
    }


    public function getAnyData()
    {
        try {
            return  $this->join('CONTATOS', 'CONTATOS.ID_PESSOA', '=', 'PESSOA.ID_PESSOA')
            ->join('LOGRADOURO', 'LOGRADOURO.ID_LOGRADOURO', '=', 'PESSOA.ID_LOGRADOURO')
            ->join('BAIRRO', 'BAIRRO.ID_BAIRRO', '=', 'LOGRADOURO.ID_BAIRRO')
            ->join('CIDADE', 'CIDADE.ID_CIDADE', '=', 'BAIRRO.ID_CIDADE')
            ->where('PESSOA.FL_CLIENTE', '1')
            ->select(['PESSOA.ID_PESSOA','CONTATOS.DS_CEL_01', 'CONTATOS.DS_TEL_01', 'CONTATOS.DS_EMAIL', 'PESSOA.NM_PESSOA', 'CIDADE.NM_CIDADE'])
            ->get();

        } catch (\Exception $e) {
            
            ( $e->getMessage() );

        }catch( \FatalErrorException $p){

            ( $p->getMessage() ); 
        }
        
    }
   
}
