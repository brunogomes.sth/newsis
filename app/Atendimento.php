<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atendimento extends Model
{
    //
    protected $table = 'ATENDIMENTO';
    public $timestamps = true;
    protected $primaryKey = 'ID_ATENDIMENTO';
    protected $fillable = ['CD_ATENDIMENTO','DT_ATENDIMENTO','ID_PESSOA',
        'ID_USUARIO','ID_STATUS','ID_PRAZO','DS_OBS','ID_USUARIO_ALT',
        'ID_ALTERACAO','ID_TERMINO'];
}
