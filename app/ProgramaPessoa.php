<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ProgramaPessoa extends Model
{
    //
    protected $table = 'PROGRAMA_PESSOA';
    public $timestamps = true;
    protected $primaryKey = 'ID_PROGRAMA_PESSOA';
    protected $fillable = ['ID_PESSOA','ID_VERSION','DT_CADASTRO','ID_USUARIO'];


    /**
    *Retorna programas por pessoa.
    *
    *@param int pessoa_id
    *@return Array
    **/
    public function getProgramasPorPessoa($pessoa_id)
    {
    	
        return DB::table('PROGRAMA_PESSOA')
                ->leftJoin('VERSION', 'VERSION.ID_VERSION', '=', 'PROGRAMA_PESSOA.ID_VERSION')
                ->leftJoin('PROGRAMA', 'PROGRAMA.ID_PROGRAMA', '=', 'VERSION.ID_PROGRAMA')
                ->where('PROGRAMA_PESSOA.ID_PESSOA', $pessoa_id)
                ->select('PROGRAMA_PESSOA.ID_PROGRAMA_PESSOA', 'PROGRAMA.NM_PROGRAMA', 'VERSION.DS_VERSION', 'PROGRAMA.DS_PLATAFORM')
                ->get();
    }
}
