<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Protocolo extends Model
{
    //
    protected $table = 'PROTOCOLO';
    public $timestamps = true;
    protected $primaryKey = 'ID_PROTOCOLO';
    protected $fillable = ['ID_ATENDIMENTO','DT_PROTOCOLO','DS_REMETENTE',
        'DS_DESTINATARIO','ID_PESSOA','DS_HISTORICO','ID_USUARIO'];
}
