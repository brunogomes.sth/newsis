<?php
if (!function_exists('mask')) {
    /**
     * Função responsavel por receber um valor e adicionar uma mascara pre-difinida
     * ex: mask($data,'##/##/####')
     */
    function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
}
if (!function_exists('arrayLetrasMaiusculas')) {
    /**
     * Função responsavel por receber um Array e Transformar as strings desse array em letras maiusculas
     */
    function arrayLetrasMaiusculas($dados)
    {
        foreach ($dados as $k => $v) {
            if (is_string($v)) {
                $res[$k] = strtoupper($v);
            } else {
                $res[$k] = $v;
            }
        }
        return $res;
    }
}


function setLogradouro($data, $pessoa)
{

    if (isset($data['auto']) && !empty($data['auto'])) {
        $data['ID_LOGRADOURO'] = \Crypt::decrypt($data['auto']);

        if (isset($pessoa) && !is_null($pessoa)) {
            $pessoa->ID_LOGRADOURO = $data['ID_LOGRADOURO'];
        } else {
            $pessoa = \App\Pessoa::create($data);
        }

    } else {

        $Bairro = \App\Bairro::create($data);
        $data['ID_BAIRRO'] = $Bairro->ID_BAIRRO;

        $Logradouro = \App\Logradouro::create($data);

        $data['ID_LOGRADOURO'] = $Logradouro->ID_LOGRADOURO;

        if (isset($pessoa) && !is_null($pessoa)) {
            $pessoa->ID_LOGRADOURO = $data['ID_LOGRADOURO'];
        } else {
            $pessoa = \App\Pessoa::create($data);
        }
    }

    return $pessoa;

}

function getPrazos()
{
    return [
        52 => 'Anual',
        02 => 'Demo',
        26 => 'Semestral',
        13 => 'Trimestral'
    ];
}