<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fisica extends Model
{
    //
    protected $table = 'FISICA';
    public $timestamps = true;
    protected $primaryKey = 'ID_FISICA';
    protected $fillable = ['ID_PESSOA','CD_CPF','CD_RG','DT_NASC','APELIDO'];


    public function getFillables()
    {
    	return array_flip($this->fillable);
    }
    
}
