<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contatos extends Model
{
    protected $table = 'CONTATOS';
    public $timestamps = true;
    protected $primaryKey = 'ID_CONTATO';
    protected $fillable = ['NM_CONTATO','DS_TEL_01','DS_TEL_02','DS_CEL_01','DS_CEL_02','DS_EMAIL', 'TP_PESSOA_CONTATO', 'ID_PESSOA'];

    public function pessoa(){
        return $this->belongsTo('App\Pessoa','ID_PESSOA','ID_PESSOA');
    }

    public function getFillables()
    {
    	return array_flip($this->fillable);
    }

}
