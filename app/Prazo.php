<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prazo extends Model
{
    //
    protected $table = 'PRAZO';
    public $timestamps = true;
    protected $primaryKey = 'ID_PRAZO';
    protected $fillable = ['CD_PRAZO','NM_PRAZO','QT_DIAS'];
}
