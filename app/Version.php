<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Version extends Model
{
    //
    protected $table = 'VERSION';
    public $timestamps = true;
    protected $primaryKey = 'ID_VERSION';
    protected $fillable = ['DS_VERSION','ID_PROGRAMA','DS_HISTORICO','DT_VERSION'];
    
    public function programa(){
        return $this->belongsTo('App\Programa', 'ID_PROGRAMA', 'ID_PROGRAMA');
    }

    /**
     * Se o cliente usar o programa com esta versao passada no pametro ele retorna o cliente que esta usando
    */
    public function clienteUsaVersao($id_versao){
        $programaPessoa = ProgramaPessoa::where('ID_VERSION',$id_versao);

        if ($programaPessoa->count() > 0){
            return $programaPessoa;
        }else{
            return false;
        }
    }
}
