<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{

    //
    protected $table = 'FUNCIONARIO';
    protected $primaryKey = 'ID_FUNCIONARIO';
    protected $fillable = ['ID_PESSOA', 'ID_CARGO', 'ID_SETOR', 'COMISSAO', 'ID_FISICA'];

    public function user(){
        return $this->belongsTo('App\User','ID_FUNCIONARIO','ID_FUNCIONARIO');
    }

    public function pessoa(){
        return $this->belongsTo('App\Pessoa','ID_PESSOA','ID_PESSOA');
    }

    public function cargo(){
        return $this->belongsTo('App\Cargo','ID_CARGO','ID_CARGO');
    }

    public function setor(){
        return $this->belongsTo('App\Setor','ID_SETOR','ID_SETOR');
    }
    
    public function getFillables()
    {
        return array_flip($this->fillable);
    }
}
