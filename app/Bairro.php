<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bairro extends Model
{
    //
    protected $table = 'BAIRRO';
    public $timestamps = true;
    protected $primaryKey = 'ID_BAIRRO';
    protected $fillable = ['ID_CIDADE','NM_BAIRRO'];


    public function cidade()
    {
    	return $this->belongsTo('App\Cidade', 'ID_CIDADE');
    }

    public function getFillables()
    {
    	return array_flip($this->fillable);
    }
}
