<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Logradouro extends Model
{
    //
    protected $table = 'LOGRADOURO';
    public $timestamps = true;
    protected $primaryKey = 'ID_LOGRADOURO';
    protected $fillable = ['DS_CEP','ID_BAIRRO','NM_LOGRADOURO','DS_TIPO'];



    public function bairro()
    {
    	return $this->belongsTo('App\Bairro', 'ID_BAIRRO');
    }

    public function pessoa()
    {
    	return $this->hasOne('App\Pessoa', 'ID_LOGRADOURO', 'Pessoa');
    }

    public function getLogradouroPorCidadeId($cidade, $logradouro){

        try {
            
            return DB::table('LOGRADOURO')
                        ->join('BAIRRO', 'BAIRRO.ID_BAIRRO' , '=', 'LOGRADOURO.ID_BAIRRO')
                        ->select( 'LOGRADOURO.ID_LOGRADOURO','LOGRADOURO.NM_LOGRADOURO','LOGRADOURO.DS_CEP', 'BAIRRO.NM_BAIRRO')
                        ->where([
                            ['BAIRRO.ID_CIDADE','=', $cidade],
                            ['LOGRADOURO.NM_LOGRADOURO','ilike',"%". $logradouro . "%" ]
                            ])
                        ->get();

        } catch (\Exception $e) {
            //Log::create($e->getMessage());        
            return ['error' => 'Contate a sistech'];
        }
        
    }


    public function getFillables()
    {
        return array_flip($this->fillable);
    }

}
