<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoSeg extends Model
{
    //
    protected $table = 'GRUPO_SEG';
    public $timestamps = true;
    protected $primaryKey = 'ID_GRUPO_SEG';
    protected $fillable = ['NM_GRUPO_SEG','DS_GRUPO_SEG'];
}
