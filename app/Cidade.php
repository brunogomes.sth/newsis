<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $table = 'CIDADE';
    public $timestamps = true;
    protected $primaryKey = 'ID_CIDADE';
    protected $fillable = ['NM_CIDADE', 'ID_ESTADO'];


    public function estado(){
        return $this->hasOne('App\Estado','ID_ESTADO','ID_ESTADO');
    }


}
