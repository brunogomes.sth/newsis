<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Senha extends Model
{
    //
    protected $table = 'SENHA';
    public $timestamps = true;
    protected $primaryKey = 'ID_SENHA';
    protected $fillable = ['ID_PESSOA','DS_SENHA_ENT','DS_SENHA_SAI','DT_RESGISTRO'];
}
