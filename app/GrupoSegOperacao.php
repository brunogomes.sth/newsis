<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoSegOperacao extends Model
{
    //
    protected $table = 'GRUPO_SEG_OPERACAO';
    public $timestamps = true;
    protected $primaryKey = 'ID_GRUPO_OPERACAO';
    protected $fillable = ['ID_GRUPO_SEG','ID_OPERACAO'];
}
