<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //
    protected $table = 'STATUS';
    public $timestamps = true;
    protected $primaryKey = 'ID_STATUS';
    protected $fillable = ['NM_STATUS'];
    
}
