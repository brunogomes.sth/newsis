<?php
/** Route Partial Map
 * -----------------------------------------------------
 * Os aquivos de Rotas estão armazenados na pasta Routes
 * Será criado um arquivo de rota para cada grupo. (EX: clientes.php)
 * Sera gravado em um array chamado $route_partials o nome dos arquivos de rotas criados
 */


$route_partials = [
    'home',
    'clientes',
    'agenda',
    'programas',
    'programa_cliente',
    'atendimento',
    'usuario',
    'funcionarios',
    'outros',
    'auth',
    'faker',
    'logradouro',
    'cidadesdobrasil',
    'teste'

];

/**
 * Route Partial Loadup
 */
foreach ($route_partials as $partial) {
    $file = __DIR__ . '/Routes/' . $partial . '.php';
    if (!file_exists($file)) {
      $msg = "Route partial [{$partial}] not found.";
    throw new \Illuminate\Contracts\Filesystem\FileNotFoundException($msg);
    }
    require_once $file;
}


