<?php
/* Rotas para controle de usuario
* rotas para autentificação de usuario */
Route::get('auth/login', 'Auth\AuthController@getLogin')->name('login');
Route::post('auth/login', 'Auth\AuthController@postLogin')->name('entrar_login');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('logout');
//rotas para registro de usuário
Route::get('auth/register', ['as' => 'novo_user', function () {
    return view('auth.register');
}]);
Route::post('auth/register', 'Auth\AuthController@postRegister')->name('registrar_user');

//Rotas para recuperação de senha
// Rotas para solicitar trocar de senha...
Route::get('password/email', 'Auth\PasswordController@getEmail')->name('getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail')->name('postEmail');

// Rotas para trocar a senha...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset')->name('getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset')->name('postReset');