<?php

Route::group(['prefix' => 'agenda'], function () {
    Route::get('/', ['as' => 'agenda', 'uses' => 'AgendaTelefonicaController@index']);
    Route::get('/verContato/{id}', ['as' => 'verContato', 'uses' => 'AgendaTelefonicaController@show']);

    Route::get('/novo', ['as' => 'novoContato', 'uses' => 'AgendaTelefonicaController@create']);
    Route::post('/salvar', ['as' => 'salvarContato', 'uses' => 'AgendaTelefonicaController@store']);

    Route::get('/edita/{id}', ['as' => 'editaContato', 'uses' => 'AgendaTelefonicaController@edit']);
    Route::post('/update/{id}', ['as' => 'updateContato', 'uses' => 'AgendaTelefonicaController@update']);

    Route::post('/anyData', ['as' => 'agenda.data', 'uses' => 'AgendaTelefonicaController@anyData']);

});
