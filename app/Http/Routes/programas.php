<?php
//rotas para trabanha com programas
Route::group(['prefix' => 'programas'], function () {
    Route::get('', ['as' => 'programas', 'uses' => 'ProgramasController@index']);
    Route::get('{id}/show', ['as' => 'vizualizacao', 'uses' => 'ProgramasController@show']);
    Route::get('add', ['as' => 'p_adicionar', 'uses' => 'ProgramasController@create']);
    Route::get('{id}/delP', ['as' => 'excluirPrograma', 'uses' => 'ProgramasController@destroy']);
    Route::get('{id}/edit', ['as' => 'editar', 'uses' => 'ProgramasController@edit']);
    Route::post('{id}/update', ['as' => 'atualizar', 'uses' => 'ProgramasController@update']);
    Route::post('', ['as' => 'salvar', 'uses' => 'ProgramasController@store']);
//routa de versão
    Route::post('/versao', ['as' => 'salvarVersao', 'uses' => 'VersionsController@store']);
    Route::get('{id}/delV', ['as' => 'excluirVersao', 'uses' => 'VersionsController@destroy']);
    Route::get('{id}/editVersao', ['as' => 'editVersao', 'uses' => 'VersionsController@edit']);
    Route::post('/anyData', ['as' => 'programa.data', 'uses' => 'ProgramasController@anyData']);
    Route::post('/{id}/updateversao', ['as' => 'updateVersao', 'uses' => 'VersionsController@update']);
});

Route::get('getPrograma/', function($id = null) {
    return response()->json(\App\Programa::select('ID_PROGRAMA', 'NM_PROGRAMA', 'DS_PLATAFORM')->distinct('ID_PROGRAMA')->orderBy('ID_PROGRAMA')->get());
});
Route::get('getVersion/{id}', function($id = null) {
    return response()->json(\App\Version::where('ID_PROGRAMA', $id)->orderBy('ID_VERSION')->get());
});