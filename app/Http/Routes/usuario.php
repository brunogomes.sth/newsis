<?php
//rotas para trabanha com Funcionario
Route::group(['prefix' => 'usuario'], function () {
    Route::get('', ['as' => 'usuario', 'uses' => 'UsuarioController@index']);
    Route::post('', ['as' => 'user_salvar', 'uses' => 'UsuarioController@store']);
    Route::get('{id}/show', ['as' => 'user_vizualizar', 'uses' => 'UsuarioController@show']);
    Route::get('{id}/{nome}/{email}/add', ['as' => 'user_adicionar', 'uses' => 'UsuarioController@create']);
    Route::get('{id}/del', ['as' => 'user_excluir', 'uses' => 'UsuarioController@destroy']);
    Route::get('{id}/edit', ['as' => 'user_editar', 'uses' => 'UsuarioController@edit']);
    Route::post('{id}/update', ['as' => 'user_atualizar', 'uses' => 'UsuarioController@update']);
    Route::post('{id}/imagem', ['as' => 'user.imagem', 'uses' => 'UsuarioController@updateAvatar']);
    Route::post('/anyData', ['as' => 'usuario.data', 'uses' => 'UsuarioController@anyData']);
});