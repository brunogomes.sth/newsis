<?php
//rotas para trabanha com Funcionario
Route::group(['prefix' => 'funcionario'], function () {
    Route::get('', ['as' => 'funcionario', 'uses' => 'FuncionariosController@index']);
    Route::post('', ['as' => 'f_salvar', 'uses' => 'FuncionariosController@store']);
    Route::get('{id}/show', ['as' => 'f_vizualizar', 'uses' => 'FuncionariosController@show']);
    Route::get('add', ['as' => 'f_adicionar', 'uses' => 'FuncionariosController@create']);
    Route::get('{id}/del', ['as' => 'f_excluir', 'uses' => 'FuncionariosController@destroy']);
    Route::get('{id}/edit', ['as' => 'f_editar', 'uses' => 'FuncionariosController@edit']);
    Route::post('{id}/update', ['as' => 'f_atualizar', 'uses' => 'FuncionariosController@update']);
    Route::post('/anyData', ['as' => 'funcionario.data', 'uses' => 'FuncionariosController@anyData']);
});