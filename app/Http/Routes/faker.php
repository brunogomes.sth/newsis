<?php



Route::get('/faker/create', function(){

	//factory(App\Bairro::class, 30)->create();
	//factory(App\Logradouro::class, 30)->create();
	
	$pessoas =	factory(App\Pessoa::class, 50)
					->create()
					->each( function($pessoa){
							factory( App\Contatos::class)->create(['ID_PESSOA' => $pessoa->ID_PESSOA]);
							factory( App\Fisica::class)->create(['ID_PESSOA' => $pessoa->ID_PESSOA]);
					} );
	//factory(App\Fisica::class, 30)->create();
	//factory(App\Contatos::class, 30)->create();



});


Route::get('/faker/clean', function(){

	\App\Bairro::truncate();
	\App\Logradouro::truncate();
	\App\Contatos::truncate();
	\App\Pessoa::truncate();




});