<?php

Route::get('/logradouro/cidade/{id}/logradouro/{nome}', function(\App\Logradouro $logradouro, $id, $nome){

	$id   = (int) $id;
	$nome = filter_var( (string) $nome, FILTER_SANITIZE_STRING);

	$ruas  = Array();
	
	foreach ($logradouro->getLogradouroPorCidadeId($id, $nome) as $k => $v) {
		$v->ID_LOGRADOURO = Crypt::encrypt($v->ID_LOGRADOURO);
		$ruas[$k] = $v	;	
	}

	return  response()->json(  (strlen($nome) < 5) ? 
								['response' => 'null'] : $ruas
								 
		);
	
});


