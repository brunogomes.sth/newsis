<?php

Route::get('/', function () {
    if (Auth::check()) {
        return view('home.home');
    }
    return view('auth.login');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', ['as' => 'home', function () {
        return view('home.home');
    }]);
});
