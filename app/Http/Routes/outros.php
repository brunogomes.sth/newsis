<?php
//rotas para trabalhando com Grupo de Cliente
Route::group(['prefix' => 'outros'], function () {

    //Rotas para grupo de clientes
    Route::get('grupoCliente', ['as' => 'gc_create', 'uses' => 'GrupoClienteController@create']);
    Route::post('grupoCliente', ['as' => 'gc_salvar', 'uses' => 'GrupoClienteController@store']);
    Route::get('grupoCliente/{id}/del', ['as' => 'gc_excluir', 'uses' => 'GrupoClienteController@destroy']);
    Route::get('grupoCliente/{id}/edit', ['as' => 'gc_editar', 'uses' => 'GrupoClienteController@edit']);
    Route::post('grupoCliente/{id}/update', ['as' => 'gc_atualizar', 'uses' => 'GrupoClienteController@update']);
    Route::post('grupoCliente/anyData', ['as' => 'grupoCliente.data', 'uses' => 'GrupoClienteController@anyData']);

    //Rotas setores
    Route::get('setor', ['as' => 'se_create', 'uses' => 'SetorController@create']);
    Route::post('setor', ['as' => 'se_salvar', 'uses' => 'SetorController@store']);
    Route::get('setor/{id}/del', ['as' => 'se_excluir', 'uses' => 'SetorController@destroy']);
    Route::get('setor/{id}/edit', ['as' => 'se_editar', 'uses' => 'SetorController@edit']);
    Route::post('setor/{id}/update', ['as' => 'se_atualizar', 'uses' => 'SetorController@update']);
    Route::post('setor/anyData', ['as' => 'setores.data', 'uses' => 'SetorController@anyData']);

    //Rotas status
    Route::get('status', ['as' => 'st_create', 'uses' => 'StatusController@create']);
    Route::post('status', ['as' => 'st_salvar', 'uses' => 'StatusController@store']);
    Route::get('status/{id}/del', ['as' => 'st_excluir', 'uses' => 'StatusController@destroy']);
    Route::get('status/{id}/edit', ['as' => 'st_editar', 'uses' => 'StatusController@edit']);
    Route::post('status/{id}/update', ['as' => 'st_atualizar', 'uses' => 'StatusController@update']);
    Route::post('status/anyData', ['as' => 'status.data', 'uses' => 'StatusController@anyData']);

    //Rotas cargos
    Route::get('cargo', ['as' => 'ca_create', 'uses' => 'CargoController@create']);
    Route::post('cargo', ['as' => 'ca_salvar', 'uses' => 'CargoController@store']);
    Route::get('cargo/{id}/del', ['as' => 'ca_excluir', 'uses' => 'CargoController@destroy']);
    Route::get('cargo/{id}/edit', ['as' => 'ca_editar', 'uses' => 'CargoController@edit']);
    Route::post('cargo/{id}/update', ['as' => 'ca_atualizar', 'uses' => 'CargoController@update']);
    Route::post('cargo/anyData', ['as' => 'cargo.data', 'uses' => 'CargoController@anyData']);
    
});