<?php
Route::group(['prefix' => 'clientes'], function () {
    Route::get('/', ['as' => 'clientes', 'uses' => 'ClienteController@index']);
    Route::get('/novocliente/', ['as' => 'novoCliente', 'uses' => 'ClienteController@create']);
    Route::get('/clientes/{id}', ['as' => 'editCliente', 'uses' => 'ClienteController@edit']);
    Route::post('/updateCliente/{id}/', ['as' => 'updateCliente', 'uses' => 'ClienteController@update']);
    Route::post('/clientes/salvar/', ['as' => 'salvarCliente', 'uses' => 'ClienteController@store']);
    Route::get('/clientes/{id}/show/', ['as' => 'showCliente', 'uses' => 'ClienteController@show']);

    Route::post('/anyData', ['as' => 'cliente.data', 'uses' => 'ClienteController@anyData']);

   
});