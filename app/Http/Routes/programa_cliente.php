<?php
Route::get('/programa-cliente/{id}/', ['as' => 'programaCliente', 'uses' => 'ProgramaClienteController@index']);
Route::post('/programa-cliente/', ['as' => 'programaClienteAdd', 'uses' => 'ProgramaClienteController@add']);
Route::get('/programa-cliente/Exclui/{id}', ['as' => 'programaClienteRemove', 'uses' => 'ProgramaClienteController@destroy']);