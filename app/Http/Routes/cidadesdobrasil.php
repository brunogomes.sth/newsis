<?php
// ----------- Rotas responsaveis por buscar no banco as cidades do brasil---------------------------
Route::get('/ufs', function ($uf = null) {
    return response()->json(\App\Estado::select('ID_ESTADO','SG_ESTADO')->distinct('SG_ESTADO')->orderBy('SG_ESTADO')->get());
});
Route::get('/cidades/{id}', function ($id = null) {
    return response()->json(\App\Cidade::where('ID_ESTADO', $id)->orderBy('NM_CIDADE')->get());
});
