<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\GrupoCliente;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Http\Controllers\OutrosCadastrosController;

class GrupoClienteController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('outros.grupoCliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $dados = arrayLetrasMaiusculas($request->all());
            if(GrupoCliente::create($dados))
            {
                $css = 'flash_message_success';
                $msg = 'Grupo de Cliente cadastrado com sucesso!';
            }
            
        } catch (Exception $e) {
                $css = 'flash_message_error';
                $msg = 'Erro ao cadastrar grupo de usuarios!';
        }

        session()->flash($css,$msg);
        return redirect()->route('gc_create');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $grupoCliente = GrupoCliente::find($id);
            return view('outros.grupoCliente', compact('grupoCliente'));
        } catch (Exception $e) {
            session()->flash('Erro ao tentar editar Grupo de Cliente.');
            return redirect()->route('se_create');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {

            $dados = $request->all();
            if(GrupoCliente::find($id)->update($dados)){
                $css = 'flash_message_success';
                $msg = 'Grupo de cliente atualizado com sucesso.';
            }    

        } catch (Exception $e) {
            $css = 'flash_message_error';
            $msg = 'Erro ao tentar atualizar grupo de clientes.';
        }
        
        return redirect()->route('gc_create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

           $id = (int) $id;
           if($id < 1) return redirect()->route('gc_create');

           if( GrupoCliente::find($id)->delete() )
           {
            $css = 'flash_message_success';
            $msg = 'Grupo de cliente deletado com sucesso.';
           }
       } catch (Exception $e) {
            $css = 'flash_message_error.';
            $msg = 'Erro ao tentar cadastrar grupo de cliente.';
       }
       
       session()->flash($css, $msg);
       return redirect()->route('gc_create');
    }

    public function anyData()
    {

        $grupoCliente = GrupoCliente::all();
        return Datatables::of($grupoCliente)
            ->addColumn('acoes', function ($grupoCliente) {

                return ('<div class="text-center">
                            <a href="'. route( 'gc_editar', ['id' => $grupoCliente->ID_GRUPO_CLIENTE  ] ) .'" 
                            class="btn btn-xs btn-primary">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                             <a href="' .route( 'gc_excluir', ['id' => $grupoCliente->ID_GRUPO_CLIENTE  ] )  . '" 
                            class="btn btn-xs btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </div>');
            })
            ->make(true);
    }
}
