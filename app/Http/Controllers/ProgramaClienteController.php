<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pessoa;
use App\ProgramaPessoa;

class ProgramaClienteController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id) {
        $id = (int) $id;

        $pro = new ProgramaPessoa; 

        $pessoas = Pessoa::find($id);
        
        return view('programa_cliente.programaCliente', 
                [
                    'pessoas' => $pessoas, 
                    'cliPrg' => $pro->getProgramasPorPessoa($id)
                ]
            );
    }

    public function add(Request $request) {

        try {

           $version = \App\Version::find($request->ID_VERSION)->first();
           if(!isset($version) || empty($version) )
           {
            $css = 'flash_message_error';
            $msg = 'Versão incorreta ou inexistente.';
           }

        unset($version);
        $cliPrg = new ProgramaPessoa( $request->input() );

        if( $cliPrg->save() ){
            $css = 'flash_message_success';
            $msg = 'Pragrama adicionado ao clinete.';

        }
        
    } catch (\Exception  $e ) {

        //Log::create($e->getMessage());

        $css = 'flash_message_error';
        $msg = 'Erro ao tentar salvar um programa ao cliente.';
    }

    session()->flash($css,$msg);   
    return redirect()->route('programaCliente', $request->ID_PESSOA);

        

     
    }

    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $pessoaId = ProgramaPessoa::where('ID_PROGRAMA_PESSOA',$id)->select('ID_PESSOA')->first();
        $cliPrg = ProgramaPessoa::find($id)->delete();
         return redirect()->route('programaCliente', $pessoaId->ID_PESSOA); 
    }

}
