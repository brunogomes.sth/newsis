<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProgramaRequest;
use App\Programa;
use App\Version;
use App\Http\Controllers\Controller;
use App\Http\Controllers\VersionsController as Versions;
use Yajra\Datatables\Datatables;
use DB;

class ProgramasController extends Controller
{

    public function index()
    {

        return view('programa.programa');
    }

    //Chama tela pra cadastro
    public function create()
    {
        return view('programa.cadPrograma');
    }

    //Grava regsitro
    public function store(ProgramaRequest $request)
    {
       
        try {
            DB::beginTransaction();
            //Pegando dados enviado pedo usuário

            //$dados = $request->all();
            # Converte strings de array request para letras maiusculas
            $dados = arrayLetrasMaiusculas($request->all());

            //Mudando o formato de data para o banco
            $dados['DT_VERSION'] = implode("-", array_reverse(explode("/", $dados['DT_VERSION'])));

            //Gravando programa no banco
            $programa = Programa::create($dados);
            //Gravando Versão no banco
            Version::create(['DS_VERSION' => $dados['DS_VERSION'],
                'ID_PROGRAMA' => $programa->ID_PROGRAMA,
                'DS_HISTORICO' => $dados['DS_HISTORICO'],
                'DT_VERSION' => $dados['DT_VERSION']]);

            DB::commit();
            session()->flash('flash_message_success', 'Cadastro realizado com sucesso!');
            return redirect()->route('programas');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('flash_message_error', 'Erro ao tentar cadastrar as informações!');
            return redirect()->route('programas');
        }
    }

    //Pega dados para vizualização
    public function show($id)
    {
        $id = (int)$id;
        try {
        //Buscando dados de programa
        $programa = Programa::find($id);

        //Mudando data para formato brasileiro
        $programa['DT_CADASTRO'] = implode("/", array_reverse(explode("-", substr($programa['DT_CADASTRO'], 0, 10))));

        //Buscando versões
        $versoes = Programa::find($id)->version;
        //Pecorrendo o array versoes e mudando as data para brasil
        $count = count($versoes);
        for ($i = 0; $i < $count; $i++) {
            $versoes[$i]['DT_VERSION'] = implode("/", array_reverse(explode("-", ($versoes[$i]['DT_VERSION']))));
        }
        }catch (\Exception $e){
            session()->flash('flash_message_error', 'Erro ao visualizar as informações do programa!');
            return redirect()->route('programas');
        }


        return view('programa.verPrograma', compact('programa'), ['versoes' => $versoes]);
    }

    //Pegando dados para editar
    public function edit($id)
    {
        $id = (int)$id;
        try {
            $dados = Programa::find($id);
            $dadosVersion = Version::select('DS_VERSION', 'DT_VERSION', 'DS_HISTORICO')->where('ID_PROGRAMA', $id)->first();
            # Converte a data da versão para o formato brasileiro
            $dadosVersion->DT_VERSION = date('d/m/Y', strtotime($dadosVersion->DT_VERSION));
            return view('programa.cadPrograma', ['dados' => $dados, 'dadosVersion' => $dadosVersion]);
        }catch (\Exception $e){
            session()->flash('flash_message_error', 'Erro ao mostrar as informações para edição!');
            return redirect()->route('programas');
        }
    }

    //Grava alteração
    public function update(ProgramaRequest $request, $id)
    {
        $id = (int)$id;
        try {
            DB::beginTransaction();
            //Dados enviado pelo usuário
            // $dados = $request->all();
            $dados = arrayLetrasMaiusculas($request->all());

            //Objeto de versao
            $version = new Versions();

            //Verifica quem vai ser alterado
            if (isset($dados['ID_PROGRAMA'])) {
                Programa::find($id)->update($dados);
            } else if (isset($dados['ID_VERSION'])) {
                $version->update($dados);
            }
            DB::commit();
            session()->flash('flash_message_success', 'Cadastro atualizado com sucesso!');
            return redirect()->route('programas');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('flash_message_error', 'Erro ao tentar atualizar informações!');
            return redirect()->route('programas');

        }

    }

    //Excluir registros
    public function destroy($id)
    {
        $id = (int)$id;
        try{
            DB::beginTransaction();
            //Deletando programa
            Programa::find($id)->delete();
            //Deletando versao desse programa

            #quando excluir um programa tem que verificar se o cliente esta usando aluma versao do programa
            $v = new Version();
            $versoes = Version::where('ID_PROGRAMA', $id)->get();
            
            foreach ($versoes as $versao){
               // dd($versao->ID_VERSION);
                $res = $v->clienteUsaVersao($versao->ID_VERSION);

                if ($res == false):
                    continue;
                else:
                    break;
                endif;
            }

            if ($res == false){

                Version::where('ID_PROGRAMA', $id)->delete();
                DB::commit();
                session()->flash('flash_message_success', 'Programa excluído com sucesso!');
            }else{
                session()->flash('flash_message_error', 'Este programa não pode ser excluído por que algum cliente está usando uma versão!');
                DB::rollBack();
            }
             return redirect()->route('programas');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('flash_message_error', 'Erro ao tentar excluir as informações!');
            return redirect()->route('programas');

        }


    }

    public function anyData()
    {
        //Carregar lista de programas
        $ListaPrograma = Programa::all();


        //Função para converta a data para o padrão brasileiro.
        foreach ($ListaPrograma as $lista) {
            $dataVersao = Programa::find($lista['ID_PROGRAMA'])->version;//pega versões

            foreach ($dataVersao as $value) {
                $lista['VERSAO'] = $value->DS_VERSION;
                $dtVersao = $value->DT_VERSION;
                $lista['DT_VERSAO'] = implode("/", array_reverse(explode("-", $dtVersao)));
            }
        }

        return Datatables::of($ListaPrograma)->addColumn('acoes', function ($ListaPrograma) {
            return ('<div class="text-center">
                        <a class="btn btn-success btn-xs" title="Vizualizar"
                        href="programas/' . $ListaPrograma->ID_PROGRAMA . '/show">
                        <span class="glyphicon glyphicon-list-alt"></span>
                        </a>

                        <a class="btn btn-primary btn-xs editar" title="Editar"
                        href=" programas/' . $ListaPrograma->ID_PROGRAMA . '/edit">
                        <span class="glyphicon glyphicon-pencil"></span>
                        </a>

                        <button type="button" class="btn btn-info btn-xs" data-toggle="modal"
                        data-target="#ModalVersao" data-id=' . $ListaPrograma->ID_PROGRAMA . '
                        data-nome=' . str_replace(' ', '|', $ListaPrograma->NM_PROGRAMA) . '>
                        
                        <i class="fa fa-plus-circle"></i> Versão
                        </button>
                    </div>');
        })->make(true);
    }
}


