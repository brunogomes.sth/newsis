<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cargo;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('outros.cargos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('outros.cargos');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try {

            $cargo = new Cargo( $request->input() );
            $cargo = arrayLetrasMaiusculas($cargo);
            if( $cargo->save() )
            {
                $css = 'flash_message_success';
                $msg = 'Cargo cadastrado com sucesso!';    
            }

        } catch (Exception $e) {
            $css = 'flash_message_error';
            $msg = 'Erro ao tentar cadastrar cargo';
        }
        
        session()->flash($css, $msg);
        return redirect()->route('ca_create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $id = (int) $id;
            if($id < 1) return redirect()->route('ca_create');
            $cargo = Cargo::find($id);
            return view('outros.cargos', compact('cargo'));
        } catch (Exception $e) {
            session()->flash('flash_message_error', 'Erro ao tentar buscar cargo');
            return redirect()->route('ca_create');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $id = (int) $id;
            if($id < 1) return redirect()->route('ca_create');

            if(Cargo::find($id)->update( $request->all() ) )
            {
                $css = 'flash_message_success';
                $msg = 'Cargo atualizado com sucesso!';
            }
            

        } catch (Exception $e) {
            $css = 'flash_message_error';
            $msg = 'Erro ao tentar atualizar cargo';
        }

        session()->flash( $css , $msg );
        return redirect()->route('ca_create');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $id = (int) $id;
            if($id < 1) return redirect()->route('ca_create');

            if(Cargo::find($id)->delete())
            {
                $css = 'flash_message_success';
                $msg = 'Cargo deletado com sucesso.';
            }

        } catch (Exception $e) {
            $css = 'flash_message_error';
            $msg = 'Erro ao tentar deletar cargo.';
        }
        
        session()->flash( $css , $msg );
        return redirect()->route('ca_create');
    }

    public function anyData()
    {
        $cargos = Cargo::all();

        return Datatables::of($cargos)
            ->addColumn('acoes', function ($cargos) {
                return ('<div class="text-center">
                            <a href="' . route( 'ca_editar', ['id' =>  $cargos->ID_CARGO  ] )  . '" 
                            class="btn btn-xs btn-primary">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                             <a href="' . route( 'ca_excluir', ['id' =>  $cargos->ID_CARGO  ] ). '" 
                            class="btn btn-xs btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </div>');
            })
            ->make(true);
    }
}
