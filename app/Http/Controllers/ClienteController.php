<?php

namespace App\Http\Controllers;

use App\Cidade;
use App\Logradouro;
use App\Contatos;
use App\Estado;
use App\Bairro;
use DB;
use App\Http\Requests;
use App\Http\Requests\ClienteRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Validator;
use App\Pessoa;
use App\Fisica;
use App\Juridica;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Yajra\Datatables\Datatables;

class ClienteController extends Controller
{
    public function index()
    {
        return view('cliente.cliente');
    }

    public function anyData(Request $r)
    {

        //dd($r->all());
        $pessoa = new Pessoa;
        $pessoas = collect($pessoa->getAnyData());

        
        return Datatables::of($pessoas)
            ->addColumn('acoes', function ($pessoas) {
                return ('<div class="text-center">
                <a href="clientes/clientes/' . $pessoas->ID_PESSOA . '/show" class="btn btn-xs btn-success" title="Visualizar"><i class="glyphicon glyphicon-list-alt"></i></a>
                <a href="clientes/clientes/' . $pessoas->ID_PESSOA . '" class="btn btn-xs btn-primary" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>
                <a href="programa-cliente/' . $pessoas->ID_PESSOA . '" class="btn btn-xs btn-info" title="Programas Cliente"><i class="fa fa-desktop"></i></a>
                </div>');
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cliente.cadCliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteRequest $request)
    {   

        try {

            DB::beginTransaction();

            #copiando os dados do request para uma variavel auxiliar
            $dados = $request->all();

            #criando uma nova instancia de pessoa e ja setando logradouro
            $Pessoa = setLogradouro( $dados, null );
            
            #verifica se o cadastro de pessoa foi realizado
            if( !$Pessoa->exists )
            {   
                session()->flash('flash_message_error', 'Erro ao tentar salvar cliente.');
                return redirect()->route('novoCliente');
            }

            #setando o id de pessoa dentro da variavel auxiliar
            $dados['ID_PESSOA'] = $Pessoa->ID_PESSOA;

            #apos termos todos os dados sera criado um novo contato

            $Contatos = Contatos::create( $dados );

            if( !$Contatos->exists )
            {
                session()->flash('flash_message_error', 'Erro ao tentar salvar contato do cliente.');
                return redirect()->route('novoCliente');
            }

            #salvando o tipo de pessoa, caso seja fisica ou juridica
            $tp_p = ($request->TP_PESSOA == '1')?  Fisica::create($dados) : Juridica::create($dados);

            if( !$tp_p->exists )
            {
                session()->flash('flash_message_error', 'Erro no cadastro de clientes!');
                return redirect()->route('novoCliente');
            } 
            
            DB::commit();

        } catch (\Exception $e) {
            print_r($e->getMessage());
            DB::rollBack();
            session()->flash('flash_message_error', 'Erro entre em contato com a sistech!');
            //return redirect()->route('novoCliente');

        }


        session()->flash('flash_message_success', 'Cliente cadastrado, cadastre seus programas!');
        return redirect()->route('programaCliente', $Pessoa->ID_PESSOA );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

        try {

            $id = (int) $id;

            if($id < 1 ){
                session()->flash('flash_message_error', 'Tente procurar um cliente válido.');
                return redirect()->route('clientes');
            } 

            $pessoas =  Pessoa::find($id); 

            if(empty($pessoas) || !isset($pessoas))
            {
                session()->flash('flash_message_error', 'O cliente buscado não foi encontrado.');
                return redirect()->route('clientes');
            }
           
            $p = new \App\ProgramaPessoa;
            return view('cliente.verCliente', 
                        [
                         'pessoas'   => $pessoas, 
                         'prazos'    => getPrazos() ,
                         'cliPrg' => $p->getProgramasPorPessoa($pessoas->ID_PESSOA)
                         ] 
                    );

        } catch (\Exception $e) {
            session()->flash('flash_message_error', 'Erro ao buscar dados do cliente.');
            return redirect()->route('clientes');
        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $id = (int) $id;

            if($id < 1 ){
                 session()->flash('flash_message_error', 'Tente procurar um cliente válido.');
                return redirect()->route('clientes');
            }

            $pessoa = Pessoa::find($id);

             if(empty($pessoa) || !isset($pessoa))
            {
                session()->flash('flash_message_error', 'O cliente buscado não foi encontrado.');
                return redirect()->route('clientes');
            }

            return view('cliente.cadcliente')->with('pessoas', $pessoa);

        } catch (\Exception $e) {
            session()->flash('flash_message_error', 'Erro ao buscar informações');
            return redirect()->route('clientes');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteRequest $request, $id)
    {

        $id = (int) $id;

        if($id < 1)
        {
            session()->flash('flash_message_error', 'Erro, tente editar um cliente valido.');
            return redirect()->route('clientes');
        }
        try {

            DB::beginTransaction();

            $data = $request->all();
        
            #editando pessoa
            $p = new Pessoa;
            $pessoa = $p->where('ID_PESSOA','=', $id);
            $pessoa->update(  array_intersect_key( $data , $p->getFillables() ) );


            /*
            se existe a chave auto então significa que foi selecionado um logradouro da base 
            e não vamos cadastro-lo navamente, caso não então vamos cadastrar um novo logradouro

            Se auto existe, então temos que buscar na base pelo registro 
            */
            $pessoa = $p->where('ID_PESSOA','=', $id )->get()[0]; 

            setLogradouro($data, $pessoa )->save();

            #tirando da memoria variaveis desnecessarias apartir daqui
            unset($p);
            unset($pessoa);
            #atualizando contato
            $c = new Contatos;
            $c->where('ID_PESSOA','=',$id)
              ->update( array_intersect_key($data, $c->getFillables() ) ) ;
                
            unset($c);

            $f = new Fisica;
            $j = new Juridica;

            $data['ID_PESSOA'] = $id;

                if($request->TP_PESSOA == '1')
                    {
                        //se caso esteja mudando de juridica para fisica, vamos deletetar juridica
                        $jexist = $j->where('ID_PESSOA', '=', $id);
                        if( $jexist->exists() )
                                 $jexist->delete() ;

                       unset($j);
                       unset($jexist);
                       //se ja existe um cadastro de fisica vamos atualiza-lo
                       $fexist = $f->where('ID_PESSOA','=', $id);
                       if( $fexist->exists() ){
                            $fexist->update( array_intersect_key($data, $f->getFillables() ));

                        unset($f);
                        unset($fexist);
                    //caso não haja vamos criar uma novo cadastro.    
                    }else{ 
                        Fisica::create($data);
                    }

                }else{
                    //se existe fisica vamos deletar
                    $fexist =  $f->where('ID_PESSOA', '=', $id);
                    if( $fexist->exists() )
                             $fexist->delete() ;

                   unset($f);
                   unset($fexist);
                   //caso ja tenha uma juridica vamos atualiza-la
                   $jexist =  $j->where('ID_PESSOA','=', $id);
                   if($jexist->exists() ){
                         $jexist->update( array_intersect_key($data, $j->getFillables() ));

                    unset($j);
                    unset($jexist); 
                    //caso não vamos cria uma nova juridica.
                    }else{ 
                        Juridica::create($data);
                    }
            
                }

            unset($data);

            DB::commit();

            session()->flash('flash_message_success', 'Cadastro atualizado com sucesso!');
            return redirect()->route('clientes');

        } catch (\Exception $e) {
            print $e->getMessage();
            DB::rollBack();
            session()->flash('flash_message_error', 'Erro ao tentar atualizar informações! ');
           // return redirect()->route('clientes');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
