<?php

namespace App\Http\Controllers;

use App\Fisica;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Yajra\Datatables\Datatables;
use App\Http\Requests\FuncionarioRequest;
use App\Funcionario;
use App\Cidade;
use App\Setor;
use App\Cargo;
use App\Pessoa;
use App\Contatos;
use App\Bairro;
use App\Logradouro;

class FuncionariosController extends Controller
{

    public function index()
    {
        return view('funcionario.funcionario');
    }

    public function create()
    {
        #pega setores e carrega a pela para cadastro
        $setores = Setor::all();
        $cargos = Cargo::all();
        return view('funcionario.cadFuncionario', ['setores' => $setores, 'cargos' => $cargos]);
    }

    public function store(FuncionarioRequest $request)
    {
        try {
            DB::beginTransaction();

            #muda o formato data para formato do banco
            $request->DT_NASC = implode("/", array_reverse(explode("-", $request->DT_NASC)));
            $dados = arrayLetrasMaiusculas($request->all());

/*
            $Bairro = Bairro::create($dados);
            $dados['ID_BAIRRO'] = $Bairro->ID_BAIRRO;

            $Logradouro = Logradouro::create($dados);
            $dados['ID_LOGRADOURO'] = $Logradouro->ID_LOGRADOURO;*/

            $Pessoa = setLogradouro( $dados, null );

            #criando uma nova instancia de pessoa
            //$Pessoa = Pessoa::create($dados);
            //unset($dados['ID_LOGRADOURO']);
            //unset($dados['ID_BAIRRO']);

            #verifica se cadastro de pessoa foi realizado
            if (!$Pessoa->exists) {
                session()->flash('flash_message_error', 'Erro ao tentar salva Funcionário');
                return redirect()->route('f_adicionar');
            }

            #setando o id de pessoa da variável auxiliar
            $dados['ID_PESSOA'] = $Pessoa->ID_PESSOA;

            $Contato = Contatos::create($dados);
            #verifica se cadastro de contato foi realizado
            if (!$Contato->exists) {
                session()->flash('flash_message_error', 'Erro ao tentar salva Contatos');
                return redirect()->route('f_adicionar');
            }
            
            $fisica = Fisica::create($dados);
            $dados['ID_FISICA'] = $fisica->ID_FISICA;
            
            $Funcionario = Funcionario::create($dados);
            #verifica se cadastro de pessoa foi realizado
            if (!$Funcionario->exists) {
                session()->flash('flash_message_error', 'Erro ao tentar salva Funcionário');
                return redirect()->route('f_adicionar');
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            session()->flash('flash_message_error', 'Erro ao tentar salva Funcionário');
        };

        session()->flash('flash_message_success', 'Cadastro realizado com sucesso!');
        return redirect()->route('f_adicionar');
    }

    public function show($id)
    {
        try {
            #pega dados de pessoa selecionado para visualização
            $pessoa = Pessoa::find($id);
            if ($pessoa == null) {
                throw new Exception('Variável pessoas não trouxe dados');
            }
            return view('funcionario.verFuncionario', compact('pessoa'));
        } catch (Exception $e) {
            session()->flash('flash_message_error', 'Erro ao buscar dados do funcionário.');
            return redirect()->route('funcionario');
        };

    }

    public function edit($id)
    {
        try {
            #pegando os dados de pessoa escolhida para edição
            $pessoa = Pessoa::find($id);
            $pessoa->fisica['DT_NASC'] = implode("/", array_reverse(explode("-", ($pessoa->fisica['DT_NASC']))));//inventerdo a data
            $setores = Setor::all();
            $cargos = Cargo::all();
            if ($pessoa == null) {
                throw new Exception('Variável funcionário não trouxe dados');
            }
            return view('funcionario.cadFuncionario',['setores' => $setores, 'cargos' => $cargos])->with('pessoa', $pessoa);
        } catch (Exception $e) {
            session()->flash('flash_message_error', 'Erro ao buscar dados do funcionário.');
            return redirect()->route('funcionario');
        };

    }

    public function update(FuncionarioRequest $request, $id)
    {
        $id = (int)$id;
        if ($id < 1) {
            session()->flash('flash_message_error', 'Erro, tenta editar em Funcionário');
            return redirect()->route('funcionario');
        }

        try {
            DB::beginTransaction();

            #muda o formato data para formato do banco
            $request->DT_NASC = implode("/", array_reverse(explode("-", $request->DT_NASC)));
            $data = $request->all();

            $p = new Pessoa;
            $Pessoa = Pessoa::where('ID_PESSOA', $id)
                ->update(array_intersect_key($data, $p->getFillables()));
            unset($p);

            $f = new Funcionario;
            $funcionario = Funcionario::where('ID_PESSOA', $id)
                ->update(array_intersect_key($data, $f->getFillables()));
            unset($f);

            $c = new Contatos;
            $contatos = Contatos::where('ID_PESSOA', $id)
                ->update(array_intersect_key($data, $c->getFillables()));
            unset($c);

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            session()->flash('flash_message_error', 'Erro ao atualizar Funcionário!');
            return redirect()->route('funcionario');
        }
        session()->flash('flash_message_success', 'Cadastro atualizado com sucesso!');
        return redirect()->route('funcionario');
    }

    public function destroy($id)
    {
        /*deleta funcionario
        Funcionario::where('ID_PESSOA', $id)->delete();
        Contatos::where('ID_PESSOA', $id)->delete();
        deleta pessoa
        Pessoa::find($id)->delete();
        return redirect()->route('funcionario');*/
    }

    public function anyData()
    {
        $pessoas = Pessoa::with(['funcionario', 'contato'])
            ->where('PESSOA.FL_FUNCIONARIO', '=', 1)
            ->get();

        return Datatables::of($pessoas)
            ->addColumn('acoes', function ($pessoas) {
                return ('<div class="text-center">
                            <a href="funcionario/' . $pessoas->ID_PESSOA . '/show" 
                            class="btn btn-xs btn-success">
                                <i class="glyphicon glyphicon-list-alt"></i>
                            </a>
                            <a href="funcionario/' . $pessoas->ID_PESSOA . '/edit" 
                            class="btn btn-xs btn-primary">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                            
                            <a href="usuario/' . $pessoas->funcionario->ID_FUNCIONARIO . '/'
                    . $pessoas->NM_PESSOA . '/' . $pessoas->contato->DS_EMAIL . '/add"
                    class="btn btn-xs btn-info">
                                <i class="glyphicon glyphicon-user"></i>
                            </a>
                        </div>');
            })
            ->make(true);
    }
}