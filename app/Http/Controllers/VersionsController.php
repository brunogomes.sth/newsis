<?php

namespace App\Http\Controllers;

use App\Programa;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Version;
use App\Http\Requests\VersionRequest;
use App\Http\Controllers\Controller;


class VersionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$ListaVersoes = Version::all();
        //return view('versions', ['ListaVersoes' => $ListaVersoes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(VersionRequest $request)
    {
        try {
            DB::beginTransaction();
            $versao = $request->all();
            $versao['DT_VERSION'] = implode("-", array_reverse(explode("/", $versao['DT_VERSION'])));
            Version::create($versao);
            DB::commit();
            session()->flash('flash_message_success', 'Versão cadastrada com sucesso!');
            return redirect()->route('programas');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('flash_message_error', 'Erro ao tentar cadastrar as informações!');
            return redirect()->route('programas');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editVers = 'S';
        $dadosVersion = Version::find($id);
        $dadosVersion['DT_VERSION'] = implode("/", array_reverse(explode("-", $dadosVersion['DT_VERSION'])));
        $dados = Programa::where('ID_PROGRAMA', $dadosVersion->ID_PROGRAMA)->first();
        return view('programa.cadPrograma', compact('dadosVersion', 'dados', 'editVers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(VersionRequest $request, $id)
    {
        $dados = $request->all();
        $dados['DT_VERSION'] = implode("-", array_reverse(explode("/", $dados['DT_VERSION'])));

        Version::find($id)->update($dados);
        session()->flash('flash_message_success', 'Versão atualizada com sucesso!');
        return redirect()->route('programas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = (int)$id;
        $v = new Version();
        $versao = Version::find($id);
        if($v->clienteUsaVersao($id) == false){

            $cont = Version::where('ID_PROGRAMA', $versao->ID_PROGRAMA)->count();
            if ($cont > 1) {
                $versao->delete();
                session()->flash('flash_message_success', 'Versão excluída com sucesso!');
            } else {
                session()->flash('flash_message_error', 'Você não pode excluir esta versão!');
            }
        }else{
            session()->flash('flash_message_error', 'Você não pode excluir esta versão! Alguns clientes estão usando esta versao');
        }

        return redirect()->route('vizualizacao', $versao->ID_PROGRAMA);
    }
}
