<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Setor;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class SetorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('outros.setores');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('outros.setores');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $dados = arrayLetrasMaiusculas($request->all());

            $css = $msg = '';
            if(Setor::create($dados)){
                $css = 'flash_message_success';
                $msg = 'Setor cadastrado com sucesso!';
            }
            
        } catch (Exception $e) {
            $css = 'flash_message_error';
            $msg = 'Erro ao cadastrar setor!';
        }

        session()->flash($css, $msg);
        return redirect()->route('se_create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = (int) $id;
            if($id < 1) return redirect()->route('outros');
            $setor = Setor::find($id);
            return view('outros.setores',compact('setor'));
        } catch (Exception $e) {
            session()->flash('Erro ao tentar editar setor.');
            return redirect()->route('se_create');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
         $id = (int) $id;
         if($id < 1) return redirect()->route('outros');

         $dados = $request->all();
         if(Setor::find($id)->update($dados))
         {
            $css = 'flash_message_success';
            $msg = 'Setor atualizado com sucesso!';
        }   
    } catch (Exception $e) {
        $css = 'flash_message_error';
        $msg = 'Erro ao tentar atualizar setor.';
    }   

    session()->flash($css,$msg );
    return redirect()->route('se_create');

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = (int) $id;
            if($id < 1) return redirect()->route('se_create');
            if(Setor::find($id)->delete())
            {
              $css = 'flash_message_success';  
              $msg =  'Setor deletado com sucesso!';  
            }
        } catch (Exception $e) {
            $css = 'flash_message_error';
            $msg = 'Erro ao tentar deletar setor.';
        }
        session()->flash($css,$msg);
        return redirect()->route('se_create');
    }

    public function anyData()
    {
        $setores = Setor::all();

        return Datatables::of($setores)
            ->addColumn('acoes', function ($setores) {
                return ('<div class="text-center">
                            <a href="' . route( 'se_editar', ['id' =>  $setores->ID_SETOR  ] ) . '" 
                            class="btn btn-xs btn-primary">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                             <a href="' . route( 'se_excluir', ['id' =>  $setores->ID_SETOR  ] ). '" 
                            class="btn btn-xs btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </div>');
            })
            ->make(true);
    }
}
