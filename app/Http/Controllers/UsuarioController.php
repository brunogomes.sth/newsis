<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;
use App\Http\Controllers\Controller;
use App\User;
use App\Funcionario;
use Yajra\Datatables\Datatables;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id,$nome,$email)
    {

        //$funcionario = Pessoa::find($id)->funcionario;
        if (Funcionario::find($id)->user){
            $usuario = Funcionario::find($id)->user;

            return redirect()->route('user_vizualizar',['id'=>$usuario->id]);
        }else{
            return view('auth.register', ['id'=>$id,'nome'=>$nome,'email'=>$email]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::find($id);

        //$funcionario = User::find($id)->funcionario;

        return view('auth.profile', compact('usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::find($id);
        return view('auth.register', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = $request->all();
        User::find($id)->update($usuario);
        session()->flash('flash_message_success', 'Cadastro atualizado com sucesso!');
        return redirect()->route('usuario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function anyData(){
        $users = User::where('id','>',1)->get();;

        return Datatables::of($users)->addColumn('acoes', function($users){
            return('<div class="text-center"> 
                      <a class="btn btn-success btn-xs" title="Vizualizar"
                        href="usuario/'.$users->id.'/show">
                        <span class="glyphicon glyphicon-list-alt"></span>
                        </a>

                        <a class="btn btn-primary btn-xs editar" title="Editar"
                        href="usuario/'.$users->id.'/edit">
                        <span class="glyphicon glyphicon-edit"></span>
                        </a>  
                   </div>');
        })->make(true);

    }

    public function updateAvatar(Request $request, $id)
    {
        $user = User::findOrFail($id);

        Storage::put(
            'avatars/'.$user->id,
            file_get_contents($request->file('imagem')->getRealPath())
        );

        return redirect()->route('user_vizualizar',['id'=>$user['id']]);
    }
}
