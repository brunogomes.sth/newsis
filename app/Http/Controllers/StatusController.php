<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Status;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('outros.status');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('outros.status');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $dados = arrayLetrasMaiusculas($request->all());

            if( Status::create($dados) )
            {
                $msg =  'Status cadastrado com sucesso!';
                $css =  'flash_message_success'; 
            }
        } catch (Exception $e) {
                $msg =  'Erro ao tentar cadastrar status!';
                $css =  'flash_message_error';           
        }
        session()->flash($css , $msg);
        return redirect()->route('st_create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = (int) $id;
        if($id < 1) return redirect()->route('outros');
        try {
            $status = Status::find($id);
            return view('outros.status', compact('status'));
        } catch (Exception $e) {
            session()->flash('flash_message_error' , 'Erro ao tentar cadastrar status!');
            return redirect()->route('st_create');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $id = (int) $id;
            if($id < 1) return redirect()->route('st_create');

            $dados = $request->all();
            if( Status::find($id)->update($dados) )
            {
                $css = 'flash_message_success';
                $msg = 'Status atualizado com sucesso!';    
            }
            
        } catch (Exception $e) {
            $css = 'flash_message_error';
            $msg = 'Erro ao tentar atualizar status!';
        }

        session()->flash($css , $msg);
        return redirect()->route('st_create');  
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $id = (int) $id;
            if($id < 1) return redirect()->route('st_create');

            if(Status::find($id)->delete()){
                $css = 'flash_message_success';
                $msg = 'Status deletado com sucesso.';
            }   
        } catch (Exception $e) {
                $css = 'flash_message_error';
                $msg = 'Error ao tentar deletar status';
        }
        
        session()->flash($css,$msg);
        return redirect()->route('st_create');
    }

    public function anyData()
    {
        $status = Status::all();

        return Datatables::of($status)
            ->addColumn('acoes', function ($status) {
                return ('<div class="text-center">
                            <a href="' . route( 'st_editar', ['id' => $status->ID_STATUS   ] )  . '" 
                            class="btn btn-xs btn-primary">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                             <a href="' . route( 'st_excluir', ['id' => $status->ID_STATUS   ] ) . '" 
                            class="btn btn-xs btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </div>');
            })
            ->make(true);
    }
}
