<?php

namespace App\Http\Controllers;

use App\Pessoa;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ContatosRequest;
use App\Http\Controllers\Controller;
use App\Contatos;
use Illuminate\Support\Facades\App;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;


class AgendaTelefonicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('agenda.agenda');
    }

    public function anyData()
    {

        $contatos = Contatos::query();
        return Datatables::of($contatos)
            ->addColumn('NM_CONTATO', function ($contatos) {
                if (in_array($contatos->TP_PESSOA_CONTATO, ['C', 'F'])) {
                    $pessoa = Pessoa::where('ID_PESSOA', $contatos->ID_PESSOA)->select(['NM_PESSOA'])->first();
                    return $pessoa->NM_PESSOA;
                } else {
                    return $contatos->NM_CONTATO;
                }
            })
            ->addColumn('TIPO_CONTATO', function ($contatos) {
                if ($contatos->TP_PESSOA_CONTATO == 'C') {
                    return 'Cliente';
                } else if ($contatos->TP_PESSOA_CONTATO == 'F') {
                    return 'Funcionário';
                } else {
                    return 'Contato';
                }
            })
            ->addColumn('DS_TEL_01', function ($contatos) {
                return mask($contatos->DS_TEL_01, '(##)####-####');
            })
            ->addColumn('DS_TEL_02', function ($contatos) {
                return mask($contatos->DS_TEL_02, '(##)####-####');
            })
            ->addColumn('DS_CEL_01', function ($contatos) {
                return mask($contatos->DS_CEL_01, '(##)#####-####');
            })
            ->addColumn('DS_CEL_02', function ($contatos) {
                return mask($contatos->DS_CEL_02, '(##)#####-####');
            })
            ->addColumn('acoes', function ($contatos) {
                if ($contatos->TP_PESSOA_CONTATO == 'C') {
                    return ('<div class="text-center"><a href="clientes/clientes/' . $contatos->ID_PESSOA . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>
                <a href="clientes/clientes/' . $contatos->ID_PESSOA . '/show" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-list-alt"></i></a>
                </div>');
                } else if ($contatos->TP_PESSOA_CONTATO == 'F') {
                    return ('<div class="text-center"><a href="funcionario/' . $contatos->ID_PESSOA . '/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>
                <a href="funcionario/' . $contatos->ID_PESSOA . '/show" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-list-alt"></i></a>
                </div>');
                } else {
                    return ('<div class="text-center"><a href="agenda/edita/' . $contatos->ID_CONTATO . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>
                <a href="agenda/verContato/' . $contatos->ID_CONTATO . '" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-list-alt"></i></a>
                </div>');
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('agenda.cadContato');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContatosRequest $request)
    {
        #variaveis auxiliar para alertar ao usuario
        $css = '';
        $msg = '';

        if( empty( $request->all() ) )
           {
            #caso n tenha dados noa requisição
             $css = 'flash_message_error'; 
             $msg = 'Tente passar dados corretos';
            
           } 

        try {
            
            if( Contatos::create( $request->all() ) )
            {
                $css = 'flash_message_success';
                $msg = 'Cadastro realizado com sucesso!';
            }   

        } catch (Exception $e) {
            
            $css = 'flash_message_error';
            $msg = 'Erro ao tentar cadastrar um contato.';
        }
        
            session()->flash($css, $msg);

        return redirect()->route('agenda');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        #casting do id
        $id = (int) $id;

        $msg = '';

        try {
            #busca pelo contato
            $contatos = Contatos::where('ID_CONTATO', $id)->first();

            #caso não exista ou seja nulo
            if(count($contatos) > 1 || is_null($contatos))
                $msg = 'Certifique-se que o contato existe.';    

        } catch (Exception $e) {
                $msg = 'Erro ao tentar buscar contato';
        }
        
        if(!empty($msg))
        {
            session()->flash('flash_message_error', $msg);
            return redirect()->route('agenda');
        }

        return view('agenda.verContato', ['contatos' => $contatos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #casting forcando as entradas como inteiro
        $id = (int) $id;    

        #caso seja vazio ou não exista redireciona para a agenda
        if(!isset($id) || empty($id)) return redirect()->route('agenda');
        $msg = '';

        try {
            #tenta buscar contato
            $contatos = Contatos::where('ID_CONTATO', $id)->first();    
            #se contato não existe ou não tiver contatos seta mensagem
            if(count($contatos) < 1 || empty($contatos))
                $msg = 'Contato inexistente';

        } catch (Exception $e) {
            #caso ocorra algum erro na busca pelo contato
            $msg = 'Erro ao tentar buscar contato';
        }
        
        #a variavel msg so sera setada caso n exista dados ou de algum erro durante a busca
        #sendo assim, manda o usuario de volta para agenda.
        if(!empty($msg)){
            session()->flash('flash_message_error',$msg);
            return redirect()->route('agenda');
        }


        return view('agenda.cadContato', ['contatos' => $contatos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContatosRequest $request, $id)
    {

        #adicionando casting, forcando as entradas desse metodo como inteiro
        $id = (int) $id;

        #caso seja vazio ou não exista então volta para a rota agenda
        if(!isset($id) || empty($id)) return redirect()->route('agenda');

        #auxiliar caso de algum erro dentro do try
        $msg = '';
        try {
            #inicia a transacao
            DB::beginTransaction();
                #realiza o update
                Contatos::where('ID_CONTATO', $id)->update($request->all());
            #commita a transação
            DB::commit();

        } catch (Exception $e) {    
            #caso algo de errado ocorra volta a transação
            DB::rollback();
            #setando mensagem de erro para o usuario
            $msg = 'Erro ao tentar atualizar contato, tente novamente.';
        }


            #caso tenha sido atualizado com sucesso, seta mensagem para o usuario
            session()->flash(
                'flash_message_success',
                (!empty($msg))? $msg : 'Cadastro atualizado com sucesso!');
        #redirect
        return redirect()->route('agenda');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
