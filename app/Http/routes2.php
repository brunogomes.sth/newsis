<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


//rotas para trabanha com programas
Route::group(['prefix' => 'programas'], function () {
    Route::get('', ['as' => 'programas', 'uses' => 'ProgramasController@index']);
    Route::get('{id}/show', ['as' => 'vizualizacao', 'uses' => 'ProgramasController@show']);
    Route::get('add', ['as' => 'adicionar', 'uses' => 'ProgramasController@create']);
    Route::get('{id}/delP', ['as' => 'excluirPrograma', 'uses' => 'ProgramasController@destroy']);
    Route::get('{id}/edit', ['as' => 'editar', 'uses' => 'ProgramasController@edit']);
    Route::post('{id}/update', ['as' => 'atualizar', 'uses' => 'ProgramasController@update']);
    Route::post('', ['as' => 'salvar', 'uses' => 'ProgramasController@store']);
//routa de versão
    Route::post('/versao', ['as' => 'salvarVersao', 'uses' => 'VersionsController@store']);
    Route::get('{id}/delV', ['as' => 'excluirVersao', 'uses' => 'VersionsController@destroy']);
    Route::get('{id}/editVersao', ['as' => 'editVersao', 'uses' => 'VersionsController@edit']);
});

//rotas para trabanha com Funcionario
Route::group(['prefix' => 'funcionario'], function () {
    Route::get('', ['as' => 'funcionario', 'uses' => 'FuncionariosController@index']);
    Route::post('', ['as' => 'f_salvar', 'uses' => 'FuncionariosController@store']);
    Route::get('{id}/show', ['as' => 'f_vizualizar', 'uses' => 'FuncionariosController@show']);
    Route::get('add', ['as' => 'f_adicionar', 'uses' => 'FuncionariosController@create']);
    Route::get('{id}/del', ['as' => 'f_excluir', 'uses' => 'FuncionariosController@destroy']);
    Route::get('{id}/edit', ['as' => 'f_editar', 'uses' => 'FuncionariosController@edit']);
    Route::post('{id}/update', ['as' => 'f_atualizar', 'uses' => 'FuncionariosController@update']);
});


Route::get('/', ['as' => 'home', function () {
    return view('home');
}]);
Route::get('/usuario', ['as' => 'usuarios', function () {
    return view('usuario', ['titulo' => 'Usuários Cadastrados']);
}]);
Route::get('/atendimento', ['as' => 'atendimento', function () {
    return view('atendimento', ['titulo' => 'Atendimentos']);
}]);

//--------------chama tela de clientes----------------------
Route::group(['prefix' => 'clientes'], function () {
    Route::get('/', ['as' => 'clientes', 'uses' => 'ClienteController@index']);
    Route::get('/novocliente/', ['as' => 'novoCliente', 'uses' => 'ClienteController@create']);
    Route::get('/clientes/{id}', ['as' => 'editCliente', 'uses' => 'ClienteController@edit']);
    Route::post('/updateCliente/{id}/', ['as' => 'updateCliente', 'uses' => 'ClienteController@update']);
    Route::post('/clientes/salvar/', ['as' => 'salvarCliente', 'uses' => 'ClienteController@store']);
    Route::get('/clientes/{id}/show/', ['as' => 'showCliente', 'uses' => 'ClienteController@show']);
});
// ----------- Rotas responsaveis por buscar no banco as cidades do brasil---------------------------
Route::get('/ufs/', function ($uf = null) {
    return response()->json(\Artesaos\Cidade::select('uf')->distinct('uf')->orderBy('uf')->get());
});
Route::get('/cidades/{uf}', function ($uf = null) {
    return response()->json(\Artesaos\Cidade::where('uf', $uf)->orderBy('nome')->get());
});
//---------------------------------------------------------------------------------------------------
// ----------- Rotas responsaveis por buscar no banco os programas cadastrados e suas versoes -------

Route::get('/programa-cliente/{id}/', ['as' => 'programaCliente', 'uses' => 'ProgramaClienteController@index']);
Route::post('/programa-cliente/', ['as' => 'programaClienteAdd', 'uses' => 'ProgramaClienteController@add']);
Route::get('/programa-cliente/Exclui/{id}', ['as' => 'programaClienteRemove', 'uses' => 'ProgramaClienteController@destroy']);

Route::get('/getPrograma/', function ($id = null) {
    return response()->json(\App\Programa::select('ID_PROGRAMA', 'NM_PROGRAMA', 'DS_PLATAFORM')->distinct('ID_PROGRAMA')->orderBy('ID_PROGRAMA')->get());
});
Route::get('/getVersion/{id}', function ($id = null) {
    return response()->json(\App\Version::where('ID_PROGRAMA', $id)->orderBy('ID_VERSION')->get());
});
//---------------------------------------------------------------------------------------------------

Route::get('/programas', ['as' => 'programas', 'uses' => 'ProgramasController@index']);
Route::post('/programas', ['as' => 'programas', 'uses' => 'ProgramasController@store']);
Route::post('/versoes', ['as' => 'versoes', 'uses' => 'VersionsController@store']);
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function () {
//
});


Route::get('/dadosapagarprogramacliente/', function () {
    DB::table('PROGRAMA_PESSOA')->truncate();

});
Route::get('/dadosapagar/', function () {
    DB::table('PESSOA')->truncate();
    DB::table('fisica')->truncate();
    DB::table('juridica')->truncate();
});

