<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClienteRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'TP_PESSOA' => 'required',
            'FL_ATIVO' => 'required',
            'ID_GRUPO_CLIENTE' => 'required',
            'NM_PESSOA' => 'required|min:3',
            'CD_RG' => 'numeric',
            'CD_CPF' => 'cpf|required_if:TP_PESSOA,1',
            'CD_CNPJ' => 'cnpj|required_if:TP_PESSOA,2',
            'CD_CGF' => 'numeric',
            'NM_RESPONSAVEL' => 'required_if:TP_PESSOA,2',
            'DS_TEL_01' => 'numeric',
            'DS_TEL_02' => 'numeric',
            'DS_CEL_01' => 'required|numeric',
            'DS_CEL_02' => 'numeric',
            'DS_EMAIL' => 'required|email',
            'FL_DAR_SENHA' => 'required',
            'FL_CONTRATO' => 'required',
            'PRAZO' => 'required',
            'NN_NUMERO'=> 'numeric'
        ];
    }

    public function attributes()
    {
        parent::attributes();
        return [
            'TP_PESSOA' => 'TIPO PESSOA',
            'FL_ATIVO' => 'ATIVO',
            'ID_GRUPO_CLIENTE' => 'GRUPO DE CLIENTE',
            'NM_PESSOA' => 'NOME',
            'CD_RG' => 'RG',
            'CD_CPF' => 'CPF',
            'CD_CNPJ' => 'CNPJ',
            'CD_CGF' => 'CGF',
            'NM_RESPONSAVEL' => 'RESPONSAVEL',
            'DS_TEL_01' => 'TELEFONE 1',
            'DS_TEL_02' => 'TELEFONE 2',
            'DS_CEL_01' => 'CELULAR 1',
            'DS_CEL_02' => 'CELULAR 2',
            'DS_EMAIL' => 'E-Mail',
            'FL_DAR_SENHA' => 'DAR SENHA',
            'FL_CONTRATO' => 'CONTRATO',
            'PRAZO' => 'PRAZO',
            'CD_VEND_REPR' => 'VENDEDOR',
            'NN_NUMERO'=> 'Nº',
            'ID_CIDADE' => 'CIDADE',
            'UF' => 'ESTADO'
                    ];
    }

}
