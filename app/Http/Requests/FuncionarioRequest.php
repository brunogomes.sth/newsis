<?php

namespace App\Http\Requests;

use App\Pessoa;
use App\Http\Requests\Request;
use Route;

class FuncionarioRequest extends Request {

    //Pega o id do funcionario para verificar CPF 
    public function getId(){
        $params = Route::current()->parameters();
        if ($params){
            $fisica = Pessoa::find($params['id'])->fisica;
            $id = $fisica['ID_FISICA'];
            return $id;
        }
        return 0;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'NM_PESSOA' => 'required|min:3',
            'DT_NASC' => 'required',
            'CD_RG' => 'numeric',
            'CD_CPF' => 'cpf|unique:FISICA,CD_CPF,'.$this->getId().',ID_FISICA',
            'ID_CIDADE' => 'required',
            'NM_BAIRRO' => 'required',
            'NM_LOGRADOURO' => 'required',
            'NN_NUMERO' => 'required',
            'DS_TEL_01' => 'numeric',
            'DS_TEL_02' => 'numeric',
            'DS_CEL_01' => 'numeric|required',
            'DS_CEL_02' => 'numeric',
            'ID_CARGO' => 'required',
            'ID_SETOR' => 'required',
            'FL_ATIVO' => 'required',
        ];
    }

    public function attributes()
    {
        parent::attributes();
        return [
            'NM_PESSOA' => 'NOME DO FUNCIONÁRIO',
            'DT_NASC' => 'DATA DE NASCIMENTO',
            'CD_RG' => 'RG',
            'CD_CPF' => 'CPF',
            'ID_CIDADE' => 'CIDADE',
            'NM_BAIRRO' => 'BAIRRO',
            'NM_LOGRADOURO' => 'LOGRADOURO',
            'NN_NUMERO' => 'NUMERO',
            'DS_TEL_01' => 'TELEFONE 1',
            'DS_TEL_02' => 'TELEFONE 2',
            'DS_CEL_01' => 'CELULAR 1',
            'DS_CEL_02' => 'CELULAR 2',
            'ID_CARGO' => 'CARGO',
            'ID_SETOR' => 'SETOR',
            'FL_ATIVO' => 'ATIVO',
        ];
    }

}
