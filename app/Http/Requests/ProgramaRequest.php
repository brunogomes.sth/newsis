<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProgramaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'CD_PROGRAMA' => 'numeric',
            'NM_PROGRAMA' => 'required',
            'DS_PLATAFORM' => 'required',
            'DS_VERSION' => 'required',
            'DT_VERSION' => 'required'
        ];
    }
    public function attributes()
    {
        parent::attributes();
        return [
            'CD_PROGRAMA' => 'CÓDIGO',
            'NM_PROGRAMA' => 'NOME DO PROGRAMA',
            'DS_PLATAFORM' => 'PLATAFORMA',
            'DS_VERSION' => 'VERSÃO',
            'DT_VERSION' => 'DATA DA VERSAO'
        ];
    }
}
