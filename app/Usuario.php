<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    //
    protected $table = 'USUARIO';
    protected $primaryKey = 'ID_USUARIO';
    protected $fillable = ['ID_PESSOA','NM_LOGIN','DS_SENHA','DT_INATIVO'];
}
