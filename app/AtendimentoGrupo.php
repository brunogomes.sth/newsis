<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtendimentoGrupo extends Model
{
    protected $table = 'ATENDIMENTO_GRUPO';
    public $timestamps = true;
    protected $primaryKey = 'ID_ATEND_GRUPO';
    protected $fillable = ['ID_ATENDIMENTO','ID_GRUPO_SEG'];
}
